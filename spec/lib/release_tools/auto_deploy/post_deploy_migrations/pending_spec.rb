# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::AutoDeploy::PostDeployMigrations::Pending do
  let!(:client) { stub_const('ReleaseTools::GitlabOpsClient', spy) }

  let(:pipelines) do
    Gitlab::PaginatedResponse.new([
      create(:pipeline, :success, :trigger, id: '123'),
      create(:pipeline, :success, :trigger, id: '456')
    ])
  end

  let(:job) { create(:job, :success, name: described_class::JOB_NAME) }
  let(:raw_post_migrations) { '20220510192117_foo.rb,20220523171107_bar.rb' }

  subject(:pending_post_migrations) { described_class.new }

  describe '#execute' do
    before do
      allow(client)
        .to receive(:pipelines)
        .and_return(pipelines)

      allow(client)
        .to receive(:pipeline_job_by_name)
        .and_return(job)

      allow(client)
        .to receive(:download_raw_job_artifact)
        .and_return(raw_post_migrations)
    end

    it 'returns the post migration artifact' do
      post_migrations =
        [
          '20220510192117_foo.rb',
          '20220523171107_bar.rb'
        ]

      expect(pending_post_migrations.execute).to match_array(post_migrations)
    end

    context 'with no pending post-migrations associated' do
      let(:raw_post_migrations) { '' }

      it 'returns an empty array' do
        expect(pending_post_migrations.execute).to match_array([])
      end
    end

    context "when the job can't be found" do
      let(:job) { nil }

      it 'returns an empty array' do
        expect(pending_post_migrations.execute).to match_array([])
      end
    end

    context "when the artifact can't be found" do
      it 'returns an empty array' do
        allow(client)
          .to receive(:download_raw_job_artifact)
          .and_raise(gitlab_error(:NotFound))

        expect(pending_post_migrations.execute).to match_array([])
      end
    end

    context 'with no pipelines associated' do
      let(:pipelines) { Gitlab::PaginatedResponse.new([]) }

      it 'returns an empty array' do
        expect(pending_post_migrations.execute).to match_array([])
      end
    end
  end
end
