# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::AutoDeploy::Version do
  describe '.match?' do
    it 'returns true for a matching version' do
      tag = '14.3.202109210620+71465b74a40.94c7bb8f59a'
      package = '14.3.202109210620-71465b74a40.94c7bb8f59a'

      expect(described_class.match?(tag)).to be(true)
      expect(described_class.match?(package)).to be(true)
    end

    it 'returns false for a non-matching version' do
      expect(described_class.match?('foo')).to be(false)
    end
  end

  describe '.new' do
    it 'raises ArgumentError for invalid string' do
      expect { described_class.new('1.2.3') }.to raise_error(ArgumentError)
    end
  end

  describe '#package?' do
    it 'returns true for a package string' do
      version = described_class.new('14.4.202110051517-15238108415.1fd92aab9de')

      expect(version).to be_package
    end

    it 'returns false for a tag string' do
      version = described_class.new('14.4.202110051517+15238108415.1fd92aab9de')

      expect(version).not_to be_package
    end
  end

  describe '#tag?' do
    it 'returns true for a tag string' do
      version = described_class.new('14.4.202110051120+10cb4e6f49a.97e25b527b7')

      expect(version).to be_tag
    end

    it 'returns false for a package string' do
      version = described_class.new('14.4.202110051120-10cb4e6f49a.97e25b527b7')

      expect(version).not_to be_tag
    end
  end

  describe '#timestamp' do
    it 'returns the timestamp' do
      version = described_class.new('14.3.202109210620-71465b74a40.94c7bb8f59a')

      expect(version.timestamp).to eq('202109210620')
    end
  end

  describe '#rails_sha' do
    it 'returns the gitlab-rails sha' do
      version = described_class.new('14.3.202109210620-71465b74a40.94c7bb8f59a')

      expect(version.rails_sha).to eq('71465b74a40')
    end
  end

  describe '#omnibus_sha' do
    it 'returns the omnibus sha' do
      version = described_class.new('14.3.202109210620-71465b74a40.94c7bb8f59a')

      expect(version.omnibus_sha).to eq('94c7bb8f59a')
    end
  end

  describe '#to_package' do
    it 'converts to a package' do
      version = described_class.new('14.4.202110051120+10cb4e6f49a.97e25b527b7')

      expect(version.to_package).to eq '14.4.202110051120-10cb4e6f49a.97e25b527b7'
    end
  end

  describe '#to_tag' do
    it 'converts to a tag' do
      version = described_class.new('14.4.202110051120-10cb4e6f49a.97e25b527b7')

      expect(version.to_tag).to eq '14.4.202110051120+10cb4e6f49a.97e25b527b7'
    end
  end
end
