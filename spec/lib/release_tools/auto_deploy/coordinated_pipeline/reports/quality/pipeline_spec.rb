# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::AutoDeploy::CoordinatedPipeline::Reports::Quality::Pipeline do
  let(:data) { create(:pipeline) }
  let(:client) { stub_const('ReleaseTools::GitlabOpsClient', spy) }

  subject(:pipeline) do
    described_class.new(data, 'gstg', 'qa:smoke-main:gstg-cny')
  end

  before do
    allow(client)
      .to receive(:pipeline_jobs)
      .and_return([create(:job), create(:job)])
  end

  describe '#web_url' do
    it 'returns web_url from the job' do
      expect(pipeline.web_url).to eq(data.web_url)
    end
  end

  describe '#jobs' do
    it 'uses the correct project' do
      expect(client)
        .to receive(:pipeline_jobs)
        .with(
          ReleaseTools::Project::Quality::Staging,
          data.id,
          scope: 'failed'
        )

      pipeline
    end

    it 'returns Job instances' do
      expect(pipeline.jobs.first)
        .to be_a(ReleaseTools::AutoDeploy::CoordinatedPipeline::Reports::Quality::Job)
    end
  end
end
