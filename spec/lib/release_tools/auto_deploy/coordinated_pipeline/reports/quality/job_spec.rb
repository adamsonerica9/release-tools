# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::AutoDeploy::CoordinatedPipeline::Reports::Quality::Job do
  let(:client) { stub_const('ReleaseTools::GitlabOpsClient', spy) }
  let(:data) { create(:job, id: 11_165_701) }

  subject(:job) do
    described_class.new(data, 'gstg', 'qa:smoke-main:gstg-cny')
  end

  describe '#web_url' do
    it 'returns web_url from the job' do
      expect(job.web_url).to eq(data.web_url)
    end
  end

  describe '#errors', vcr: { cassette_name: 'quality/job_failure' } do
    it 'uses the respective quality project' do
      expect(client)
        .to receive(:job_trace)
        .with(
          ReleaseTools::Project::Quality::Staging,
          data.id
        )

      job.errors
    end

    it 'returns the spec error' do
      expect(job.errors).to include('Create Merge request push options')
    end
  end

  describe '#title' do
    it 'returns the name of the spec that failed', vcr: { cassette_name: 'quality/job_failure' } do
      expect(job.title)
        .to eq('qa/specs/features/api/3_create/merge_request/push_options_labels_spec.rb')
    end

    context 'with multiple failures on the same spec' do
      let(:data) { create(:job, id: 11_066_057) }

      it 'returns the first failure', vcr: { cassette_name: 'quality/job_multiple_failures' } do
        expect(job.title)
          .to eq('qa/specs/features/ee/browser_ui/10_govern/group/group_audit_logs_1_spec.rb')
      end
    end
  end
end
