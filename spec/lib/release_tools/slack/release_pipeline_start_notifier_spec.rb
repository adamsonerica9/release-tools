# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Slack::ReleasePipelineStartNotifier do
  let(:user_name) { 'foo' }
  let(:pipeline_url) { 'https://example.gitlab.com/pipeline/1234' }
  let(:local_time) { Time.utc(2023, 5, 14, 15, 55, 0) }
  let(:stage) { :start }
  let(:release_type) { :security }

  let(:context_elements) do
    [
      { text: ':clock1: 2023-05-14 15:55 UTC', type: 'mrkdwn' }
    ]
  end

  subject(:pipeline_notifier) { described_class.new(stage: stage, release_type: release_type) }

  around do |ex|
    ClimateControl.modify(CI_PIPELINE_URL: pipeline_url, GITLAB_USER_LOGIN: user_name) do
      ex.run
    end
  end

  before do
    allow(ReleaseTools::Slack::Message)
      .to receive(:post)
      .and_return({ 'ok' => true, 'channel' => 'channel' })
  end

  context 'with the start stage' do
    it 'posts a message' do
      message = "Release manager #{user_name} has started a #{release_type} release, initial steps will now be run in #{pipeline_url}."
      full_content = ":security-tanuki: #{message}"

      expect(ReleaseTools::Slack::Message)
        .to receive(:post)
        .with(
          {
            channel: ReleaseTools::Slack::F_UPCOMING_RELEASE,
            message: message,
            blocks: slack_mrkdwn_block(text: full_content, context_elements: context_elements)
          }
        )

      Timecop.freeze(local_time) do
        pipeline_notifier.execute
      end
    end
  end

  context 'with a finalize stage' do
    let(:stage) { :finalize }

    it 'posts a message' do
      message = "Release manager #{user_name} has started the final steps of a #{release_type} release, these steps will be executed as part of #{pipeline_url}."
      full_content = ":security-tanuki: #{message}"

      expect(ReleaseTools::Slack::Message)
        .to receive(:post)
        .with(
          {
            channel: ReleaseTools::Slack::F_UPCOMING_RELEASE,
            message: message,
            blocks: slack_mrkdwn_block(text: full_content, context_elements: context_elements)
          }
        )

      Timecop.freeze(local_time) do
        pipeline_notifier.execute
      end
    end
  end
end
