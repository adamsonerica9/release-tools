# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Security::CvesIssue do
  let(:issue) do
    build(:issue, title: 'Bypass some auth', description: File.read('spec/fixtures/cve_issue_description.md'))
  end

  subject { described_class.new(issue) }

  it 'returns title' do
    expect(subject.title).to eq('Bypass some auth')
  end

  it 'returns cvss_string' do
    expect(subject.cvss_string).to eq('AV:N/AC:L/PR:L/UI:R/S:C/C:L/I:H/A:N')
  end

  it 'returns vulnerability_description' do
    expect(subject.vulnerability_description).to eq('Vulnerability description')
  end

  it 'returns credit' do
    expect(subject.credit).to eq('Thanks [someone](https://example.com/foo) for reporting this vulnerability through our bug bounty program')
  end

  it 'returns yaml' do
    expect(subject.yaml).to include(
      {
        'reporter' => { 'name' => 'GitLab Security Team', 'email' => 'security@example.com' },
        'vulnerability' => include({ 'impact' => 'AV:N/AC:L/PR:L/UI:R/S:C/C:L/I:H/A:N' })
      }
    )
  end

  context 'when yaml is not present' do
    let(:issue) { build(:issue, title: 'Bypass some auth', description: '') }

    it 'raises error' do
      expect { subject.yaml }.to raise_error(described_class::YamlMissingError, 'CVE issue does not have YAML')
    end
  end

  context 'when yaml is not a Hash' do
    let(:issue) { build(:issue, title: 'Bypass some auth', description: "```yaml\na\n```") }

    it 'raises error' do
      expect { subject.yaml }.to raise_error(described_class::YamlInvalidError, 'CVE issue YAML is not a Hash')
    end
  end

  context 'when yaml loading fails' do
    let(:issue) { build(:issue, title: 'Bypass some auth', description: "```yaml\na: something, b: other\n```") }

    it 'raises error' do
      expect { subject.yaml }.to raise_error(described_class::YamlParseError, 'CVE issue contains invalid YAML')
    end
  end
end
