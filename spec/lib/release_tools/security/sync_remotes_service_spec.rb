# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Security::SyncRemotesService do
  describe '#execute' do
    let(:other_args) { ['master', { create_mr_on_failure: true }] }
    let(:service) { described_class.new }
    let(:notifier) { instance_double(ReleaseTools::Slack::ReleaseJobEndNotifier, send_notification: true) }

    before do
      disable_feature(:switch_to_main_branch)
      disable_feature(:merge_train_to_canonical)
    end

    it 'syncs master branch for every project and notifies slack' do
      expect(service).to receive(:sync_branches)
        .with(ReleaseTools::Project::GitlabEe, *other_args)

      expect(service).to receive(:sync_branches)
        .with(ReleaseTools::Project::GitlabCe, *other_args)

      expect(service).to receive(:sync_branches)
        .with(ReleaseTools::Project::OmnibusGitlab, *other_args)

      expect(service).to receive(:sync_branches)
        .with(ReleaseTools::Project::Gitaly, *other_args)

      expect(service).to receive(:sync_branches)
        .with(ReleaseTools::Project::HelmGitlab, *other_args)

      expect(service).to receive(:sync_branches)
        .with(ReleaseTools::Project::GitlabOperator, *other_args)

      expect(service).to receive(:sync_branches)
        .with(ReleaseTools::Project::GitlabPages, *other_args)

      expect(ReleaseTools::Slack::ReleaseJobEndNotifier).to receive(:new)
        .with(job_type: 'Sync remotes', status: :success, release_type: :security)
        .and_return(notifier)

      service.execute
    end

    context 'with merge_train_to_canonical_enabled' do
      it 'syncs master branch for all projects except GitLab Ee' do
        enable_feature(:merge_train_to_canonical)

        expect(service).not_to receive(:sync_branches)
          .with(ReleaseTools::Project::GitlabEe, *other_args)

        expect(service).to receive(:sync_branches)
          .with(ReleaseTools::Project::GitlabCe, *other_args)

        expect(service).to receive(:sync_branches)
          .with(ReleaseTools::Project::OmnibusGitlab, *other_args)

        expect(service).to receive(:sync_branches)
          .with(ReleaseTools::Project::Gitaly, *other_args)

        expect(service).to receive(:sync_branches)
          .with(ReleaseTools::Project::HelmGitlab, *other_args)

        expect(service).to receive(:sync_branches)
          .with(ReleaseTools::Project::GitlabOperator, *other_args)

        expect(service).to receive(:sync_branches)
          .with(ReleaseTools::Project::GitlabPages, *other_args)

        expect(ReleaseTools::Slack::ReleaseJobEndNotifier).to receive(:new)
          .with(job_type: 'Sync remotes', status: :success, release_type: :security)
          .and_return(notifier)

        service.execute
      end
    end

    context 'with an error' do
      it 'sends a slack failure notification and raises CouldNotSyncError' do
        enable_feature(:merge_train_to_canonical)
        expect(service).to receive(:sync_branches).and_raise(StandardError)

        expect(ReleaseTools::Slack::ReleaseJobEndNotifier).to receive(:new)
          .with(job_type: 'Sync remotes', status: :failed, release_type: :security)
          .and_return(notifier)

        expect { service.execute }.to raise_error(described_class::CouldNotSyncError)
      end
    end
  end
end
