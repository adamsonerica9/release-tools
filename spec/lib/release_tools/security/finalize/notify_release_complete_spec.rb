# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Security::Finalize::NotifyReleaseComplete do
  let(:versions) do
    [
      ReleaseTools::Version.new('16.1.3'),
      ReleaseTools::Version.new('16.0.8'),
      ReleaseTools::Version.new('15.11.13')
    ]
  end

  let(:client) { stub_const('ReleaseTools::GitlabOpsClient', spy) }
  let(:slack_notifier) { stub_const('ReleaseTools::Slack::ReleaseJobEndNotifier', spy) }

  let(:blog_post_validator) do
    stub_const('ReleaseTools::Security::Finalize::BlogPostValidator', spy)
  end

  let(:link) do
    'https://about.gitlab.com/releases/2023/07/19/security-release-gitlab-16-1-2-released/'
  end

  subject(:notifier) { described_class.new }

  before do
    allow(ReleaseTools::Versions)
      .to receive(:next_versions)
      .and_return(versions)

    allow(blog_post_validator)
      .to receive(:execute)
      .and_return(link)
  end

  describe '#execute' do
    it 'triggers a ChatOps pipeline and sends a Slack notification' do
      chatops_message =
        ":mega: GitLab Security Release: 16.1.2, 16.0.7, 15.11.12 has just been released: #{link}! Share this release blog post with your network to ensure broader visibility across our community."

      expect(client)
        .to receive(:create_pipeline)
        .with(
          ReleaseTools::Project::ChatOps,
          TRIGGER_NOTIFY: true,
          CHAT_INPUT: chatops_message
        )

      expect(slack_notifier)
        .to receive(:send_notification)

      notifier.execute
    end

    context 'with an invalid blog post link' do
      it 'raises an error' do
        allow(blog_post_validator)
          .to receive(:execute)
          .and_raise

        expect(client)
          .not_to receive(:create_pipeline)

        expect(slack_notifier)
          .to receive(:send_notification)

        expect { notifier.execute }
          .to raise_error(described_class::CouldNotCompleteError)
      end
    end
  end
end
