# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Security::Finalize::NotifyNextReleaseManagers do
  let(:client) { stub_const('ReleaseTools::GitlabClient', spy) }

  let(:tracking_issue) do
    create(
      :issue,
      due_date: '2022-05-22',
      web_url: 'https://test.com/foo/bar/-/issues/1'
    )
  end

  let(:notifier) do
    stub_const('ReleaseTools::Slack::ReleaseJobEndNotifier', spy)
  end

  let(:schedule) do
    stub_const('ReleaseTools::ReleaseManagers::Schedule', spy)
  end

  subject(:notify_next_release_managers) { described_class.new }

  describe '#execute' do
    before do
      allow(ReleaseTools::ReleaseManagers::Schedule)
        .to receive(:new)
        .and_return(schedule)

      allow(client)
        .to receive(:next_security_tracking_issue)
        .and_return(tracking_issue)
    end

    it 'posts a message on the tracking issue' do
      allow(schedule)
        .to receive(:usernames_for_version)
        .and_return([double('user1', username: 'foo'), double('user2', username: 'bar')])

      allow(schedule)
        .to receive(:version_for_date)
        .and_return("version")

      expect(client)
        .to receive(:create_issue_note)
        .with(
          ReleaseTools::Project::GitlabEe,
          { issue: tracking_issue, body: instance_of(String) }
        )

      expect(notifier)
          .to receive(:send_notification)

      without_dry_run do
        notify_next_release_managers.execute
      end
    end

    context 'when something goes wrong' do
      it 'sends a slack notification and raises exception' do
        allow(client)
          .to receive(:create_issue_note)
          .and_raise(::Gitlab::Error::Error)

        expect(notifier)
          .to receive(:send_notification)

        without_dry_run do
          expect { notify_next_release_managers.execute }
            .to raise_error(described_class::CouldNotNotifyError)
        end
      end
    end

    context 'with a dry-run' do
      it 'does nothing' do
        expect(client)
          .not_to receive(:create_issue_note)

        expect(notifier)
          .not_to receive(:send_notification)

        notify_next_release_managers.execute
      end
    end
  end
end
