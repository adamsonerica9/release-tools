# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Security::Finalize::BlogPostValidator do
  let(:version) { ReleaseTools::Version.new('16.1.2') }

  let(:current_date) { Time.utc(2023, 7, 19, 15, 55, 0) }
  let(:blog_post_link) do
    'https://about.gitlab.com/releases/2023/07/19/security-release-gitlab-16-1-2-released/'
  end

  subject(:validator) { described_class.new(version: version) }

  describe '#execute' do
    context 'with a valid blog post' do
      it 'validates the blog post link' do
        stub_request(:get, blog_post_link).to_return(body: 'foo', status: 200)

        Timecop.freeze(current_date) do
          expect(validator.execute).to eq(blog_post_link)
        end
      end
    end

    context 'with a valid critical security release blog post' do
      it 'builds the blog post link' do
        link = blog_post_link.gsub('security-release', 'critical-security-release')

        stub_request(:get, link).to_return(body: 'foo', status: 200)

        ClimateControl.modify(SECURITY: 'critical') do
          Timecop.freeze(current_date) do
            expect(validator.execute).to eq(link)
          end
        end
      end
    end

    context 'with an invalid blog post' do
      it 'raises an exception' do
        stub_request(:get, blog_post_link).to_return(body: 'foo', status: 400)

        Timecop.freeze(current_date) do
          expect { validator.execute }
            .to raise_error(described_class::CouldNotFindBlogPostError)
        end
      end
    end
  end
end
