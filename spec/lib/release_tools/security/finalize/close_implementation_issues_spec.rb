# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Security::Finalize::CloseImplementationIssues do
  let(:client) { stub_const('ReleaseTools::GitlabClient', spy) }
  let(:issue_crawler) { stub_const('ReleleaseTools::Security::IssueCrawler', spy) }
  let(:notifier) { stub_const('ReleaseTools::Slack::ReleaseJobEndNotifier', spy) }

  let(:close_implementation_issues) { described_class.new }

  let(:issue1) do
    double(
      :issue,
      iid: 1,
      project_id: 1,
      processed?: true,
      web_url: 'https://gitlab.com/gitlab-org/security/gitlab/-/issues/1'
    )
  end

  let(:issue2) do
    double(
      :issue,
      iid: 2,
      project_id: 1,
      processed?: true,
      web_url: 'https://gitlab.com/gitlab-org/security/gitlab/-/issues/2'
    )
  end

  let(:issue3) do
    double(
      :issue,
      iid: 3,
      project_id: 1,
      processed?: true,
      web_url: 'https://gitlab.com/gitlab-org/security/gitlab/-/issues/3'
    )
  end

  let(:issues) do
    [issue1, issue2, issue3]
  end

  before do
    allow(ReleaseTools::Security::IssueCrawler)
      .to receive(:new)
      .and_return(issue_crawler)

    allow(issue_crawler)
      .to receive(:upcoming_security_issues_and_merge_requests)
      .and_return([issue1, issue2, issue3])
  end

  describe '#execute' do
    context 'with processed implementation issues' do
      it 'closes the issues' do
        expect(client).to receive(:close_issue)
          .exactly(3).times

        expect(notifier)
          .to receive(:send_notification)

        without_dry_run do
          close_implementation_issues.execute
        end
      end
    end

    context 'with unprocessed implementation issues' do
      let(:issue3) do
        double(
          :issue,
          iid: 3,
          project_id: 1,
          processed?: false,
          web_url: 'https://gitlab.com/gitlab-org/security/gitlab/-/issues/3'
        )
      end

      it 'only closes issues that are processed' do
        expect(client)
          .to receive(:close_issue)
          .twice

        expect(client)
          .not_to receive(:close_issue)
          .with(1, 3)

        expect(notifier)
          .to receive(:send_notification)

        without_dry_run do
          close_implementation_issues.execute
        end
      end
    end

    context 'with no implementation issues' do
      it 'does nothing' do
        allow(issue_crawler)
          .to receive(:upcoming_security_issues_and_merge_requests)
          .and_return([])

        expect(client).not_to receive(:close_issue)

        without_dry_run do
          close_implementation_issues.execute
        end
      end
    end

    context 'with a security release pipeline' do
      around do |ex|
        env = {
          'SECURITY_RELEASE_PIPELINE' => 'true',
          'CI_JOB_URL' => 'https://example.com/foo/bar/-/jobs/1'
        }

        ClimateControl.modify(env) do
          ex.run
        end
      end

      it 'closes the issues and sends a slack notification' do
        expect(client)
          .to receive(:close_issue)
          .exactly(3).times

        expect(notifier)
          .to receive(:send_notification)

        without_dry_run do
          close_implementation_issues.execute
        end
      end

      context 'when there is a failure' do
        it 'sends a slack notification' do
          allow(client)
            .to receive(:close_issue)
            .and_raise(::Gitlab::Error::Error)

          expect(client)
            .to receive(:close_issue)

          expect(notifier)
            .to receive(:send_notification)

          without_dry_run do
            expect { close_implementation_issues.execute }
              .to raise_error(described_class::CouldNotCompleteError)
          end
        end
      end
    end
  end
end
