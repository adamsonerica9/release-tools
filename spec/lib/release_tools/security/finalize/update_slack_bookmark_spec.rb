# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Security::Finalize::UpdateSlackBookmark do
  let(:client) { stub_const('ReleaseTools::GitlabClient', spy) }
  let(:notifier) { stub_const('ReleaseTools::Slack::ReleaseJobEndNotifier', spy) }

  let(:tracking_issue) do
    create(
      :issue,
      web_url: 'https://test.com/foo/bar/-/issues/1'
    )
  end

  let(:bookmark_list) do
    {
      'bookmarks' => [
        { 'id' => 1, 'title' => 'foo' },
        { 'id' => 2, 'title' => described_class::BOOKMARK_TITLE }
      ]
    }
  end

  subject(:update_bookmark) { described_class.new.execute }

  describe '#execute' do
    before do
      allow(client)
        .to receive(:next_security_tracking_issue)
        .and_return(tracking_issue)
    end

    it 'updates the slack bookmark' do
      allow(ReleaseTools::Slack::Bookmark)
        .to receive(:list)
        .with(
          channel: ReleaseTools::Slack::RELEASES
        ).and_return(bookmark_list)

      allow(ReleaseTools::Slack::Bookmark)
        .to receive(:edit)
        .with(
          channel: ReleaseTools::Slack::RELEASES,
          bookmark: 2,
          link: tracking_issue.web_url
        )

      expect(notifier).to receive(:new)
        .with(
          job_type: 'Update slack bookmark',
          status: :success,
          release_type: :security
        )

      without_dry_run do
        update_bookmark
      end
    end

    context 'when the bookmark is not found' do
      let(:bookmark_list) do
        {
          'bookmarks' => [
            { 'id' => 1, 'title' => 'foo' },
            { 'id' => 2, 'title' => 'bar' }
          ]
        }
      end

      it 'sends a slack notification and raises exception' do
        expect(ReleaseTools::Slack::Bookmark)
          .to receive(:list)
          .with(
            channel: ReleaseTools::Slack::RELEASES
          ).and_return(bookmark_list)

        expect(notifier).to receive(:new)
          .with(
            job_type: 'Update slack bookmark',
            status: :failed,
            release_type: :security
          )

        without_dry_run do
          expect { update_bookmark }
            .to raise_error(described_class::CouldNotNotifyError)
        end
      end
    end

    context 'when the tracking issue is not found' do
      before do
        allow(client)
          .to receive(:next_security_tracking_issue)
          .and_return(nil)
      end

      it 'sends a slack notification and raises exception' do
        expect(notifier).to receive(:new)
          .with(
            job_type: 'Update slack bookmark',
            status: :failed,
            release_type: :security
          )

        without_dry_run do
          expect { update_bookmark }
            .to raise_error(described_class::CouldNotNotifyError)
        end
      end
    end

    context 'when something else goes wrong' do
      it 'sends a slack notification and raises exception' do
        expect(client)
          .to receive(:next_security_tracking_issue)
          .and_raise

        expect(notifier).to receive(:new)
          .with(
            job_type: 'Update slack bookmark',
            status: :failed,
            release_type: :security
          )

        without_dry_run do
          expect { update_bookmark }
            .to raise_error(StandardError)
        end
      end
    end

    context 'with a dry-run' do
      it 'does nothing' do
        expect(ReleaseTools::Slack::Bookmark)
          .not_to receive(:edit)

        expect(notifier)
          .not_to receive(:new)

        update_bookmark
      end
    end
  end
end
