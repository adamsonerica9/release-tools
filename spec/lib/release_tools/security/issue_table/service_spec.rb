# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Security::IssueTable::Service do
  subject(:execute) { instance.execute }

  let(:instance) { described_class.new(client) }

  let(:mr1) do
    build(
      :merge_request,
      id: 1,
      project_id: 1
    )
  end

  let(:release_issue) do
    build(:issue, id: 1, project_id: 1)
  end

  let(:issues_crawler) do
    instance_double(
      ReleaseTools::Security::IssueCrawler,
      upcoming_security_issues_and_merge_requests: issues,
      release_issue: release_issue
    )
  end

  let(:issue1) { build(:issue, project_id: 1) }
  let(:issues) { [build(:security_implementation_issue, issue: issue1, merge_requests: [mr1])] }
  let(:client) { instance_spy(ReleaseTools::Security::Client) }
  let(:validator) { instance_double(ReleaseTools::Security::MergeRequestsValidator) }

  before do
    allow(ReleaseTools::Security::IssueCrawler)
      .to receive(:new)
      .and_return(issues_crawler)

    allow(ReleaseTools::GitlabClient).to receive(:issue_notes).and_return(double(auto_paginate: []))

    allow(ReleaseTools::GitlabClient).to receive(:merge_requests).and_return([])

    allow(ReleaseTools::Security::MergeRequestsValidator)
      .to receive(:new)
      .and_return(validator)

    allow(validator)
      .to receive(:execute)
      .and_return([[], []])
  end

  describe '#execute' do
    context 'with no existing table' do
      it 'calls create_issue_note' do
        allow(ReleaseTools::GitlabClient)
          .to receive(:create_issue_note)
          .with(release_issue.project_id, { issue: release_issue, body: instance_of(String) })
      end
    end

    context 'when table exists' do
      let(:mr2) do
        build(
          :merge_request,
          id: 2,
          project_id: 1
        )
      end

      let(:issue2) { build(:issue, project_id: 1) }

      let(:issues) do
        [
          build(:security_implementation_issue, issue: issue1, merge_requests: [mr1]),
          build(:security_implementation_issue, issue: issue2, merge_requests: [mr2])
        ]
      end

      let(:existing_note_body) do
        <<~EOF
          ## Security issues

          | Issue                          | Master merged?     | Deployed? | Backports merged? | Bot Comments | Release manager comments |
          | ------------------------------ | ------------------ | ----------| ----------------- | ------------ | ------------------------ |
          | #{issue1.web_url} - ~severity::2 | | | | | Some comment   |
          | #{issue2.web_url}  | :white_check_mark:  |   | ||   Some other comment

          ---

          :robot: <sub>This table was generated by [release-tools](https://gitlab.com/gitlab-org/release-tools/).
          Please open an issue in the [Delivery team issue tracker](https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues)
          if you have any suggestions or bug reports.</sub>
        EOF
      end

      before do
        allow(ReleaseTools::GitlabClient)
          .to receive(:issue_notes)
          .and_return(double(auto_paginate: [
            build(
              :note,
              id: 1,
              noteable_id: release_issue.id,
              body: existing_note_body,
              author: build(:user, username: ReleaseTools::Security::Client::RELEASE_TOOLS_BOT_USERNAME)
            )
          ]))
      end

      it 'parses release manager comments' do
        allow(ReleaseTools::GitlabClient).to receive(:edit_issue_note)

        actual_release_manager_comments = instance.instance_variable_get(:@release_manager_comments)

        without_dry_run { execute }

        expect(actual_release_manager_comments).to eq(
          {
            issue2.web_url => 'Some other comment',
            issue1.web_url => 'Some comment'
          }
        )
      end

      it 'calls edit_issue_note' do
        expect(ReleaseTools::GitlabClient)
          .to receive(:edit_issue_note)
          .with(release_issue.project_id, { issue_iid: release_issue.iid, note_id: 1, body: instance_of(String) })

        without_dry_run { execute }
      end
    end

    context 'with dry run' do
      it 'does not post comment' do
        expect(ReleaseTools::GitlabClient)
          .not_to receive(:create_issue_note)

        expect(ReleaseTools::GitlabClient)
          .not_to receive(:edit_issue_note)

        execute
      end
    end
  end
end
