# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Security::TargetIssuesProcessor do
  let(:client) { class_spy(ReleaseTools::GitlabClient) }
  let(:crawler) { instance_spy(ReleaseTools::Security::IssueCrawler) }
  let(:notifier) { stub_const('ReleaseTools::Security::TargetIssueNotifier', spy) }
  let(:processor) { described_class.new }

  before do
    allow(processor).to receive(:client).and_return(client)
    allow(processor).to receive(:issue_crawler).and_return(crawler)
  end

  describe '#execute' do
    let(:security_release_tracking_issue) { create(:issue) }
    let(:issue_id) { 3 }
    let(:issue_response) { create(:issue, id: issue_id) }
    let(:security_implementation_issue1) { create(:security_implementation_issue, issue: issue_response) }
    let(:evaluable_issues) { [security_implementation_issue1] }

    subject(:process_issues) { processor.execute }

    before do
      allow(ReleaseTools::Issue).to receive(:new).and_return(security_release_tracking_issue)
      allow(crawler).to receive(:evaluable_security_issues).and_return(evaluable_issues)
    end

    context 'when there are no evaluable issues' do
      let(:evaluable_issues) { [] }

      it 'exits early' do
        expect(processor).not_to receive(:linked_security_issues)

        expect(process_issues).to be_nil
      end
    end

    context 'issue is ready for release' do
      before do
        allow(security_implementation_issue1).to receive(:ready_to_be_processed?).and_return(true)
      end

      it 'links issues that are ready for release' do
        allow(crawler).to receive(:related_security_issues).and_return([])

        expect(client).to receive(:link_issues).with(security_release_tracking_issue, security_implementation_issue1)
          .and_return(true)

        expect(notifier).to receive(:notify).with(:linked, security_implementation_issue1)
          .and_return(true)

        without_dry_run do
          process_issues
        end
      end

      it 'does nothing if the issue is already linked' do
        allow(crawler).to receive(:related_security_issues).and_return(evaluable_issues)

        expect(client).not_to receive(:link_issues)

        process_issues
      end

      context 'dry run' do
        it 'does not link issues that are ready' do
          allow(crawler).to receive(:related_security_issues).and_return([])

          expect(client).not_to receive(:link_issues)

          process_issues
        end
      end
    end

    context 'issue is not ready for release' do
      before do
        allow(security_implementation_issue1).to receive(:ready_to_be_processed?).and_return(false)
      end

      it 'unlinks issues that are no longer ready for release and removes the security-target label' do
        issue_link_id = 4

        allow(crawler).to receive(:related_security_issues).and_return(evaluable_issues)

        expect(client).to receive(:issue_links)
          .with(described_class::TRACKING_ISSUE_PROJECT, security_release_tracking_issue.iid)
          .and_return(
            [
              double(:issue_link, id: 1, issue_link_id: 2),
              double(:issue_link, id: issue_id, issue_link_id: issue_link_id)
            ]
          )

        expect(client).to receive(:delete_issue_link).with(security_release_tracking_issue, issue_link_id)

        expect(notifier).to receive(:notify).with(:unlinked, security_implementation_issue1)
          .and_return(true)

        expect(client).to receive(:update_issue).with(
          security_implementation_issue1.issue,
          security_implementation_issue1.project
        )

        without_dry_run do
          process_issues
        end
      end

      it 'adds a comment and removes the security-target label if the issue is not linked' do
        allow(crawler).to receive(:related_security_issues).and_return([])

        expect(client).not_to receive(:delete_issue_link)

        expect(notifier).to receive(:notify).with(:not_ready, security_implementation_issue1)
          .and_return(true)

        expect(client).to receive(:update_issue).with(
          security_implementation_issue1.issue,
          security_implementation_issue1.project
        )

        without_dry_run do
          process_issues
        end
      end

      context 'dry run' do
        it 'does not unlink issues' do
          allow(crawler).to receive(:related_security_issues).and_return(evaluable_issues)

          expect(client).not_to receive(:delete_issue_link)

          process_issues
        end
      end
    end
  end
end
