# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Security::SecurityMirrorNotificationService do
  subject(:service) { described_class.new }

  describe '#execute' do
    let(:slack_service) do
      instance_spy(
        ReleaseTools::Slack::Security::MirrorMessage,
        general_status: 'foo',
        job_status: 'bar',
        synced_repositories?: true
      )
    end

    let(:env) do
      {
        'SLACK_TOKEN' => 'token',
        'CHAT_CHANNEL' => 'channel',
        'GITLAB_TOKEN' => 'token'
      }
    end

    let(:project) do
      {
        'path_with_namespace' => 'gitlab-org/security/gitlab',
        'forked_from_project' => {
          'avatar_url' => 'avatar.png',
          'name' => 'GitLab',
          'path_with_namespace' => 'gitlab-org/gitlab'
        }
      }
    end

    let(:mirror_status) do
      instance_double(
        ReleaseTools::Security::MirrorStatus,
        available?: true,
        canonical: project['forked_from_project'],
        mirror_chain: 'Mirror chain',
        security_error: 'Security failed',
        build_error: 'Build failed',
        complete?: false
      )
    end

    let(:fake_client) { class_spy(ReleaseTools::GitlabClient) }

    around do |ex|
      # These checks only apply to CI
      ClimateControl.modify(env) do
        ex.run
      end
    end

    before do
      allow(service)
        .to receive(:security_mirrors)
        .and_return([mirror_status])

      allow(ReleaseTools::Slack::Security::MirrorMessage)
        .to receive(:new)
        .and_return(slack_service)
    end

    it 'reports the mirror status' do
      expect(slack_service)
        .to receive(:general_status)

      service.execute
    end

    context 'when running the security release pipeline' do
      let(:env) do
        {
          'SLACK_TOKEN' => 'token',
          'CHAT_CHANNEL' => 'channel',
          'GITLAB_TOKEN' => 'token',
          'SECURITY_RELEASE_PIPELINE' => 'true',
          'CI_JOB_URL' => 'https://example.com/foo/bar/-/jobs/1'
        }
      end

      it 'reports the job status' do
        expect(slack_service).to receive(:job_status)

        service.execute
      end

      context 'when the mirror check fails' do
        it 'raises an exception' do
          expect(slack_service).to receive(:synced_repositories?).and_return(false)
          expect(slack_service).to receive(:job_status)

          expect { service.execute }
            .to raise_error(described_class::RepositoriesOutOfSync)
        end
      end
    end
  end
end
