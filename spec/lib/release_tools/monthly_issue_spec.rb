# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::MonthlyIssue do
  let(:version) { ReleaseTools::Version.new('8.3.0') }

  it_behaves_like 'issuable #initialize'

  subject(:issue) { described_class.new(version: version) }

  describe '#title' do
    let(:version) { ReleaseTools::Version.new('8.3.5-rc1') }

    it "returns the issue title" do
      expect(issue.title).to eq 'Release 8.3'
    end
  end

  describe '#description' do
    before do
      allow(ReleaseTools::Versions)
        .to receive(:current_stable_branch)
        .and_return('15-11-stable-ee')
    end

    it "includes the version number" do
      expect(issue.description).to include("Tag `8.3.0`")
    end

    it "includes the correct gitlab instance" do
      content = issue.description

      expect(content).to include('[release environment](https://release.gitlab.net/help)')
      expect(content).to include('/chatops run deploy 8.3.0-ee.0 release')
      expect(content).to include('`/chatops run release check [MR_URL] 8.3`')
    end
  end

  describe '#labels' do
    it 'returns a list of labels' do
      expect(issue.labels).to eq 'Monthly Release,team::Delivery'
    end
  end

  describe '#assignees' do
    let(:version) { ReleaseTools::Version.new('11.8') }

    it 'returns the assignee IDs' do
      schedule = instance_spy(ReleaseTools::ReleaseManagers::Schedule)

      allow(ReleaseTools::ReleaseManagers::Schedule)
        .to receive(:new)
        .and_return(schedule)

      expect(schedule)
        .to receive(:active_release_managers)
        .and_return([double('user1', id: 1), double('user2', id: 2)])

      expect(issue.assignees).to eq([1, 2])
    end
  end

  describe '#current_stable_branch' do
    let(:version) { ReleaseTools::Version.new('16.0') }

    it 'returns the current stable branch' do
      versions = [
        ReleaseTools::Version.new('15.11.1'),
        ReleaseTools::Version.new('15.10.9'),
        ReleaseTools::Version.new('15.9.8')
      ]

      allow(ReleaseTools::Versions)
        .to receive(:next_versions)
        .and_return(versions)

      expect(issue.current_stable_branch).to eq('15-11-stable-ee')
    end
  end

  describe '#monthly_release_pipeline?' do
    context 'when the feature flag is enabled' do
      before do
        enable_feature(:monthly_release_pipeline)
      end

      it { is_expected.to be_monthly_release_pipeline }
    end

    context 'when the feature flag is disabled' do
      before do
        disable_feature(:monthly_release_pipeline)
      end

      it { is_expected.not_to be_monthly_release_pipeline }
    end
  end

  describe "#create" do
    before do
      enable_feature(:monthly_release_pipeline)
    end

    it 'creates a monthly release pipeline' do
      expect(issue).to receive(:monthly_release_pipeline).and_return(true)

      expect(ReleaseTools::GitlabClient).to receive(:create_issue)
        .with(issue, issue.project).and_return(true)

      issue.create
    end

    context 'monthly_release_pipeline disabled' do
      before do
        disable_feature(:monthly_release_pipeline)
      end

      it 'does not create a pipeline' do
        expect(issue).not_to receive(:monthly_release_pipeline)

        expect(ReleaseTools::GitlabClient).to receive(:create_issue)
          .with(issue, issue.project).and_return(true)

        issue.create
      end
    end
  end

  describe "#monthly_release_pipeline" do
    let(:web_url) { "https://gitlab.example.com/pipelines/123" }

    it "creates a monthly release pipeline on the ops instance" do
      expect(ReleaseTools::GitlabOpsClient).to receive(:create_pipeline).with(
        ReleaseTools::Project::ReleaseTools,
        MONTHLY_RELEASE_PIPELINE: 'true'
      ).and_return(double(web_url: web_url))

      expect(issue.monthly_release_pipeline.web_url).to eq(web_url)
    end
  end

  describe '#release_date' do
    it 'fetches the release date and returns a Date' do
      release_date = '2023-05-24'

      upcoming_releases = {
        '8.2' => '2023-04-22',
        '8.3' => release_date,
        '8.4' => '2023-06-18',
        '8.5' => '2023-07-21'
      }

      allow(ReleaseTools::GitlabReleasesClient)
       .to receive(:upcoming_releases)
       .and_return(upcoming_releases)

      expect(issue.release_date).to eq(Date.parse(release_date))
    end
  end

  describe '#formated_date' do
    it 'formats the date' do
      expect(issue.formated_date(Date.parse('2023-08-03'))).to eq('Thursday, Aug 3')
    end
  end

  describe '#ordinalized_release_date' do
    before do
      allow(issue).to receive(:release_date).and_return(Date.parse('2023-08-03'))
    end

    it 'returns the 22nd' do
      expect(issue.ordinalized_release_date).to eq('22nd')
    end

    context 'dynamic_release_date feature flag enabled' do
      before do
        enable_feature(:dynamic_release_date)
      end

      it 'returns the dynamic ordinalized day' do
        expect(issue.ordinalized_release_date).to eq('3rd')
      end
    end
  end

  context 'release day helpers' do
    before do
      allow(issue).to receive(:release_date).and_return(Date.parse('2023-08-17'))
    end

    describe '#release_day' do
      it 'returns the 22nd' do
        expect(issue.release_day).to eq('22nd')
      end

      context 'when the feature flag is enabled' do
        before do
          enable_feature(:dynamic_release_date)
        end

        it 'returns the release day' do
          expect(issue.release_day).to eq('Thursday, Aug 17')
        end
      end
    end

    describe '#tag_day' do
      it 'returns the 21st' do
        expect(issue.tag_day).to include('21st')
      end

      context 'when the feature flag is enabled' do
        before do
          enable_feature(:dynamic_release_date)
        end

        it 'returns dynamic day' do
          expect(issue.tag_day).to eq('Wednesday, Aug 16')
        end
      end
    end

    describe '#rc_tag_day' do
      it 'returns the 19th' do
        expect(issue.rc_tag_day).to include('20th')
      end

      context 'when the feature flag is enabled' do
        before do
          enable_feature(:dynamic_release_date)
        end

        it 'returns dynamic day' do
          expect(issue.rc_tag_day).to eq('Tuesday, Aug 15')
        end
      end
    end

    describe '#candidate_selection_day' do
      it 'returns the 18th' do
        expect(issue.candidate_selection_day).to include('18th')
      end

      context 'when the feature flag is enabled' do
        before do
          enable_feature(:dynamic_release_date)
        end

        it 'returns dynamic day' do
          expect(issue.candidate_selection_day).to eq('Monday, Aug 14')
        end
      end
    end

    describe '#preparation_start_day' do
      it 'returns the 17th' do
        expect(issue.preparation_start_day).to include('17th')
      end

      context 'when the feature flag is enabled' do
        before do
          enable_feature(:dynamic_release_date)
        end

        it 'returns dynamic day' do
          expect(issue.preparation_start_day).to eq('Friday, Aug 11')
        end
      end
    end
  end

  describe '#dynamic_release_date?' do
    context 'when the feature flag is enabled' do
      before do
        enable_feature(:dynamic_release_date)
      end

      it { is_expected.to be_dynamic_release_date }
    end

    context 'when the feature flag is disabled' do
      before do
        disable_feature(:dynamic_release_date)
      end

      it { is_expected.not_to be_dynamic_release_date }
    end
  end
end
