# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::PatchRelease::SecurityIssue do
  let(:versions) do
    [
      ReleaseTools::Version.new('12.7.4'),
      ReleaseTools::Version.new('12.6.6'),
      ReleaseTools::Version.new('12.5.9')
    ]
  end

  before do
    coordinator = instance_double(
      ReleaseTools::PatchRelease::Coordinator,
      versions: versions
    )

    allow(ReleaseTools::PatchRelease::Coordinator)
      .to receive(:new)
      .and_return(coordinator)
  end

  subject(:issue) { described_class.new }

  it_behaves_like 'issuable #initialize'

  describe '#title' do
    context 'with a regular security release' do
      it 'includes all the versions' do
        expect(issue.title).to eq('Security patch release: 12.7.4, 12.6.6, 12.5.9')
      end
    end

    context 'with a critical security release' do
      it 'includes the critical part' do
        allow(ReleaseTools::SharedStatus)
          .to receive(:critical_security_release?)
          .and_return(true)

        expect(issue.title).to eq('Critical security patch release: 12.7.4, 12.6.6, 12.5.9')
      end
    end
  end

  describe '#confidential?' do
    it 'is always confidential' do
      expect(issue).to be_confidential
    end
  end

  describe '#labels' do
    it 'includes the "security" label' do
      expect(issue.labels).to eq 'Monthly Release,security'
    end
  end

  describe '#description' do
    let(:tracking_issue) { double('issue', web_url: 'http://example.com') }
    let(:security_release_pipeline) { double('pipeline', web_url: 'http://example.com') }

    before do
      allow(issue).to receive(:security_release_tracking_issue).and_return(tracking_issue)
      allow(issue).to receive(:security_pipeline).and_return(security_release_pipeline)
    end

    it 'includes a step to create the blog post in a private snippet' do
      content = issue.description

      expect(content).to include 'Please create a blog post MR on gitlab-org/security/www-gitlab-com.'
    end

    it 'includes a step to perform a security release' do
      content = issue.description

      expect(content).to include '/chatops run release tag --security 12.7.4'
      expect(content).to include '/chatops run release tag --security 12.6.6'
      expect(content).to include '/chatops run release tag --security 12.5.9'
    end

    it 'includes a step to publish the packages' do
      content = issue.description

      expect(content).to include '/chatops run publish --security 12.7.4'
      expect(content).to include '/chatops run publish --security 12.6.6'
      expect(content).to include '/chatops run publish --security 12.5.9'
    end

    it 'includes the correct instance for packages' do
      content = issue.description

      expect(content).to include 'release.gitlab.net'
    end
  end

  describe '#version' do
    it 'returns the highest version' do
      expect(issue.version).to eq('12.7.4')
    end

    context 'with unsorted versions' do
      let(:versions) do
        [
          ReleaseTools::Version.new('12.6.6'),
          ReleaseTools::Version.new('12.7.4'),
          ReleaseTools::Version.new('12.5.9')
        ]
      end

      it 'returns the highest version' do
        expect(issue.version).to eq('12.7.4')
      end
    end
  end

  describe '#critical?' do
    context 'with a regular security release' do
      it { is_expected.not_to be_critical }
    end

    context 'with a critical security release' do
      before do
        allow(ReleaseTools::SharedStatus)
          .to receive(:critical_security_release?)
          .and_return(true)
      end

      it { is_expected.to be_critical }
    end
  end

  describe '#regular?' do
    context 'with a regular security release' do
      it { is_expected.to be_regular }
    end

    context 'with a critical security release' do
      before do
        allow(ReleaseTools::SharedStatus)
          .to receive(:critical_security_release?)
          .and_return(true)
      end

      it { is_expected.not_to be_regular }
    end
  end

  describe "#create" do
    it 'creates a security release pipeline' do
      expect(issue).to receive(:security_pipeline).and_return(true)

      expect(ReleaseTools::GitlabClient).to receive(:create_issue)
        .with(issue, issue.project).and_return(true)

      issue.create
    end
  end

  describe "#security_pipeline" do
    let(:web_url) { "https://gitlab.example.com/pipelines/123" }

    it "creates a security release pipeline on the ops instance" do
      expect(ReleaseTools::GitlabOpsClient).to receive(:create_pipeline).with(
        ReleaseTools::Project::ReleaseTools,
        SECURITY_RELEASE_PIPELINE: 'true',
        SECURITY: nil
      ).and_return(double(web_url: web_url))

      expect(issue.security_pipeline.web_url).to eq(web_url)
    end

    context 'critical security release' do
      it "creates a security release injecting the SECURITY variable" do
        expect(ReleaseTools::GitlabOpsClient).to receive(:create_pipeline).with(
          ReleaseTools::Project::ReleaseTools,
          SECURITY_RELEASE_PIPELINE: 'true',
          SECURITY: 'critical'
        ).and_return(double(web_url: web_url))

        ClimateControl.modify('SECURITY' => 'critical') do
          expect(issue.security_pipeline.web_url).to eq(web_url)
        end
      end
    end
  end

  describe '#projects_list' do
    it 'returns a comma separated string list of the managed versioning projects' do
      expect(issue.projects_list).to eq('cng-ee, gitaly, gitlab-pages, omnibus-gitlab-ee')
    end
  end

  describe '#security_release_tracking_issue' do
    it 'returns the security release tracking issue' do
      issue_double = double('Issue')
      crawler_double = double('IssueCrawler', release_issue: issue_double)
      expect(::ReleaseTools::Security::IssueCrawler).to receive(:new)
        .and_return(crawler_double)

      expect(issue.security_release_tracking_issue).to eq(issue_double)
    end
  end
end
