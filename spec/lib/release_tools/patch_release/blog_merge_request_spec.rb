# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::PatchRelease::BlogMergeRequest do
  let(:project) { ReleaseTools::Project::WWWGitlabCom }
  let(:version_str) { '9.1.3' }
  let(:hyphenated_version) { '9-1-3' }
  let(:source_branch) { 'create-9-1-3-post' }
  let(:blog_file_path) { "sites/uncategorized/source/releases/posts/#{blog_post_filename}" }
  let(:blog_post_filename) { "#{Date.current}-gitlab-#{hyphenated_version}-released.html.md" }

  let(:content) do
    [
      {
        version: ReleaseTools::Version.new('9.1.3'),
        pressure: 1,
        merge_requests: {
          'gitlab-org/gitlab' => [],
          'gitlab-org/gitaly' => [],
          'gitlab-org/omnibus-gitlab' => [{ 'title' => 'baz', 'web_url' => 'https://baz.com' }]
        }
      }
    ]
  end

  subject(:merge_request) { described_class.new(project: project, content: content) }

  shared_context 'multi patch blog content' do
    let(:content) do
      [
        {
          version: ReleaseTools::Version.new('9.2.2'),
          pressure: 2,
          merge_requests: {
            'gitlab-org/gitlab' => [
              { 'title' => 'foo', 'web_url' => 'https://foo.com' },
              { 'title' => 'bar', 'web_url' => 'https://bar.com' }
            ],
            'gitlab-org/gitaly' => [],
            'gitlab-org/omnibus-gitlab' => []
          }
        },
        {
          version: ReleaseTools::Version.new('9.1.3'),
          pressure: 1,
          merge_requests: {
            'gitlab-org/gitlab' => [],
            'gitlab-org/gitaly' => [],
            'gitlab-org/omnibus-gitlab' => [{ 'title' => 'baz', 'web_url' => 'https://baz.com' }]
          }
        },
        {
          version: ReleaseTools::Version.new('9.0.4'),
          pressure: 0,
          merge_requests: {
            'gitlab-org/gitlab' => [],
            'gitlab-org/gitaly' => [],
            'gitlab-org/omnibus-gitlab' => []
          }
        }
      ]
    end
  end

  it_behaves_like 'issuable #initialize'

  describe '#create' do
    context 'with three versions' do
      include_context 'multi patch blog content'

      let(:version_str) { '9.2.2' }
      let(:source_branch) { 'create-9-2-2-post' }
      let(:hyphenated_version) { '9-2-2' }

      it_behaves_like 'issuable #create', :create_merge_request do
        let(:commit_actions) do
          [{
            action: 'create',
            file_path: blog_file_path,
            content: File.read('spec/fixtures/merge_requests/patch_blog_merge_request_with_mr_lists.html.md')
          }]
        end

        before do
          allow(ReleaseTools::GitlabClient)
            .to receive(:find_or_create_branch)
            .with(source_branch, project.default_branch, project)

          allow(ReleaseTools::GitlabClient)
            .to receive(:create_commit)
            .with(project.path, source_branch, "Adding #{version_str} blog post", commit_actions)
            .and_return(instance_double(Gitlab::ObjectifiedHash))
        end

        around do |ex|
          ClimateControl.modify(USER: 'user1') { ex.run }
        end
      end
    end

    context 'with one version' do
      let(:version_str) { '9.1.3' }
      let(:source_branch) { 'create-9-1-3-post' }
      let(:hyphenated_version) { '9-1-3' }

      it_behaves_like 'issuable #create', :create_merge_request do
        let(:commit_actions) do
          [{
            action: 'create',
            file_path: blog_file_path,
            content: File.read('spec/fixtures/merge_requests/single_patch_blog_merge_request_template.html.md')
          }]
        end

        before do
          allow(ReleaseTools::GitlabClient)
            .to receive(:find_or_create_branch)
            .with(source_branch, project.default_branch, project)

          allow(ReleaseTools::GitlabClient)
            .to receive(:create_commit)
            .with(project.path, source_branch, "Adding #{version_str} blog post", commit_actions)
            .and_return(instance_double(Gitlab::ObjectifiedHash))
        end

        around do |ex|
          ClimateControl.modify(USER: 'user1') { ex.run }
        end
      end
    end
  end

  it 'has an informative Draft title', :aggregate_failures do
    expect(merge_request.title).to eq "Draft: Adding #{version_str} blog post"
  end

  describe '#labels' do
    it 'are set correctly on the MR' do
      expect(merge_request.labels).to eq 'patch release post'
    end
  end

  describe '#source_branch' do
    it 'adds the patch version to branch name' do
      expect(merge_request.source_branch).to eq source_branch
    end
  end

  describe '#target_branch' do
    it 'sets target_branch to the default branch' do
      expect(merge_request.target_branch).to eq project.default_branch
    end
  end

  describe '#assignee_ids' do
    it 'returns the current RM user IDs' do
      schedule = instance_spy(
        ReleaseTools::ReleaseManagers::Schedule,
        active_release_managers: [double('user1', id: 1), double('user2', id: 2)]
      )

      allow(ReleaseTools::ReleaseManagers::Schedule)
        .to receive(:new)
        .and_return(schedule)

      expect(merge_request.assignee_ids).to eq([1, 2])
    end
  end

  describe '#description' do
    before do
      issue = instance_spy(ReleaseTools::PatchRelease::Issue, url: 'https://dummy-issue.url')
      allow(subject).to receive(:patch_issue).and_return(issue)
    end

    it 'includes a link to the release issue' do
      without_dry_run do
        expect(merge_request.description).to include 'https://dummy-issue.url'
      end
    end

    it 'explains that the MR is adding a blog post for the patch release' do
      expect(merge_request.description).to include "Add blog post for #{version_str} patch release."
    end
  end

  describe '#patch_issue_url' do
    it 'finds patch issue and returns url' do
      issue = ReleaseTools::PatchRelease::Issue.new
      allow(ReleaseTools::PatchRelease::Issue)
        .to receive(:new)
        .and_return(issue)

      allow(issue)
        .to receive(:remote_issuable)
        .and_return(build(:issue, web_url: 'https://dummy-issue.url'))

      without_dry_run do
        expect(merge_request.patch_issue_url).to eq('https://dummy-issue.url')
      end
    end
  end

  describe '#blog_post_filename' do
    it 'returns blog post filename' do
      expect(merge_request.blog_post_filename).to eq(blog_post_filename)
    end
  end

  describe '#generate_blog_content' do
    include_context 'multi patch blog content'

    around do |ex|
      ClimateControl.modify(USER: 'user1') { ex.run }
    end

    it 'returns blog post content' do
      expected_content = File.read('spec/fixtures/merge_requests/patch_blog_merge_request_with_mr_lists.html.md')

      expect(merge_request.generate_blog_content).to eq(expected_content)
    end

    context 'with no content' do
      let(:content) { nil }

      it 'raises an error' do
        expect { merge_request.generate_blog_content }.to raise_error(StandardError)
      end
    end
  end
end
