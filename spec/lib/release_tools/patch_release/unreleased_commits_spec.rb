# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::PatchRelease::UnreleasedCommits do
  let(:version) { ReleaseTools::Version.new('15.5.5') }
  let(:project) { ReleaseTools::Project::GitlabEe }
  let(:commit_list) { create_list(:commit, 3, parent_ids: %w(bar baz)) }

  subject(:unreleased_commits) do
    described_class.new(version, project)
  end

  describe '#execute' do
    it 'returns list of unreleased commits' do
      comparison = create(:comparison, commits: commit_list)

      allow(ReleaseTools::GitlabClient)
        .to receive(:compare)
        .and_return(comparison)

      expect(unreleased_commits.execute).to eq(commit_list)
    end

    context 'with different types of commits' do
      it 'includes only merge commits' do
        commits = commit_list + [create(:commit)]
        comparison = create(:comparison, commits: commits)

        allow(ReleaseTools::GitlabClient)
          .to receive(:compare)
          .and_return(comparison)

        expect(unreleased_commits.execute).to eq(commit_list)
      end
    end

    context 'with an invalid version' do
      it 'returns an empty list' do
        allow(version)
          .to receive(:valid?)
          .and_return(false)

        expect(unreleased_commits.execute).to eq([])
      end
    end

    context 'with invalid tags or branches' do
      it 'returns an empty list' do
        allow(ReleaseTools::GitlabClient)
          .to receive(:compare)
          .with(
            project,
            a_hash_including(to: '15-5-stable-ee', from: 'v15.5.5-ee')
          ).and_raise(gitlab_error(:NotFound, code: 404))

        expect(unreleased_commits.execute).to eq([])
      end
    end
  end

  describe '#total_pressure' do
    it 'returns the number of unreleased commits' do
      allow(unreleased_commits)
        .to receive(:execute)
        .and_return(commit_list)

      expect(unreleased_commits.total_pressure).to eq(commit_list.count)
    end
  end
end
