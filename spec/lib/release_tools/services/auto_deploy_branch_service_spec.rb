# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Services::AutoDeployBranchService do
  let(:internal_client) { double('ReleaseTools::GitlabClient') }
  let(:internal_client_ops) { spy('ReleaseTools::GitlabOpsClient') }
  let(:branch_commit) { double(:commit, id: '1234') }

  subject(:service) { described_class.new('branch-name') }

  before do
    stub_const('ReleaseTools::GitlabClient', internal_client)
    stub_const('ReleaseTools::GitlabOpsClient', internal_client_ops)
  end

  describe '#create_branches!' do
    it 'creates auto-deploy branches' do
      branch_name = 'branch-name'

      allow(service).to receive(:branch_too_far_behind?).and_return(false)

      expect(service).to receive(:latest_successful_ref)
        .and_return(branch_commit.id)
        .exactly(3)
        .times
      expect(internal_client).to receive(:find_or_create_branch).with(
        branch_name,
        branch_commit.id,
        ReleaseTools::Project::GitlabEe.auto_deploy_path
      )
      expect(internal_client).to receive(:find_or_create_branch).with(
        branch_name,
        branch_commit.id,
        ReleaseTools::Project::OmnibusGitlab.auto_deploy_path
      )
      expect(internal_client).to receive(:find_or_create_branch).with(
        branch_name,
        branch_commit.id,
        ReleaseTools::Project::CNGImage.auto_deploy_path
      )

      expect(internal_client_ops).to receive(:update_variable).with(
        'gitlab-org/release/tools',
        'AUTO_DEPLOY_BRANCH',
        branch_name
      )

      results =
        without_dry_run do
          service.create_branches!
        end

      expected_projects = described_class::PROJECTS.map(&:auto_deploy_path)
      expect(results.map(&:project)).to contain_exactly(*expected_projects)

      expect(results.map(&:branch).uniq).to contain_exactly(branch_name)
    end

    it 'uses the latest commit when the auto_deploy_tag_latest feature flag is enabled' do
      branch_name = 'branch-name'
      commit = double(:commit, id: '123')
      commits_spy = instance_double(ReleaseTools::Commits)

      enable_feature(:auto_deploy_tag_latest)

      allow(ReleaseTools::Commits)
        .to receive(:new)
        .and_return(commits_spy)

      allow(commits_spy)
        .to receive(:next_commit)
        .with(commit.id)
        .and_return(nil)

      expect(commits_spy).to receive(:latest)
        .and_return(commit)
        .exactly(3)
        .times

      expect(internal_client).to receive(:find_or_create_branch).with(
        branch_name,
        commit.id,
        ReleaseTools::Project::GitlabEe.auto_deploy_path
      )

      expect(internal_client).to receive(:find_or_create_branch).with(
        branch_name,
        commit.id,
        ReleaseTools::Project::OmnibusGitlab.auto_deploy_path
      )

      expect(internal_client).to receive(:find_or_create_branch).with(
        branch_name,
        commit.id,
        ReleaseTools::Project::CNGImage.auto_deploy_path
      )

      expect(internal_client_ops).to receive(:update_variable).with(
        'gitlab-org/release/tools',
        'AUTO_DEPLOY_BRANCH',
        branch_name
      )

      without_dry_run do
        service.create_branches!
      end
    end

    it 'raises when no latest commit is found' do
      commits_spy = instance_spy(ReleaseTools::Commits)

      allow(ReleaseTools::Commits).to receive(:new).and_return(commits_spy)

      allow(commits_spy).to receive(:latest_successful_on_build).and_return(nil)
      allow(commits_spy).to receive(:find_last_auto_deploy_limit_sha).and_return(nil)

      expect do
        without_dry_run { service.create_branches! }
      end.to raise_error(RuntimeError)
    end

    context 'when next commit is more than 7 days old' do
      it 'returns notify_branch_too_far_behind true' do
        allow(internal_client).to receive(:find_or_create_branch)
        allow(ReleaseTools::PassingBuild).to receive(:new).and_return(double(next_commit: nil, for_auto_deploy_branch: double(id: nil)))

        passing_build1 = instance_spy(
          ReleaseTools::PassingBuild,
          next_commit: double(committed_date: 7.1.days.ago.iso8601),
          for_auto_deploy_branch: double(id: nil)
        )

        passing_build2 = instance_spy(
          ReleaseTools::PassingBuild,
          next_commit: double(committed_date: 6.days.ago.iso8601),
          for_auto_deploy_branch: double(id: nil)
        )

        allow(ReleaseTools::PassingBuild)
          .to receive(:new)
          .with('master', ReleaseTools::Project::GitlabEe)
          .and_return(passing_build1)
          .once

        allow(ReleaseTools::PassingBuild)
          .to receive(:new)
          .with('master', ReleaseTools::Project::CNGImage)
          .and_return(passing_build2)
          .once

        results =
          without_dry_run do
            service.create_branches!
          end

        res1 = results.detect { |r| r.project == ReleaseTools::Project::GitlabEe.auto_deploy_path }
        expect(res1.notify_branch_too_far_behind).to be(true)

        res2 = results.detect { |r| r.project == ReleaseTools::Project::OmnibusGitlab.auto_deploy_path }
        expect(res2.notify_branch_too_far_behind).to be(false)

        res3 = results.detect { |r| r.project == ReleaseTools::Project::CNGImage.auto_deploy_path }
        expect(res3.notify_branch_too_far_behind).to be(false)
      end
    end
  end
end
