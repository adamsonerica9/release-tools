# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Promotion::Checks::IncidentRollback do
  def comparison_stub(safe: true, current: nil, target: nil)
    current ||= build(:omnibus_deployment).ref
    target ||= build(:omnibus_deployment).ref

    current = ReleaseTools::AutoDeploy::Version.new(current)
    target = ReleaseTools::AutoDeploy::Version.new(target)

    instance_double(
      ReleaseTools::Rollback::Comparison,
      current: current,
      target: target,
      safe?: safe
    )
  end

  describe '#fine?' do
    it 'gracefully handles a nil comparison' do
      stub_const('ReleaseTools::Rollback::CompareService', spy(execute: nil))

      instance = described_class.new

      expect(instance).not_to be_fine
    end
  end

  describe '#to_issue_body' do
    it 'shows as available' do
      instance = described_class.new

      comparison = comparison_stub
      allow(instance).to receive(:comparison).and_return(comparison)

      presenter = instance_spy(ReleaseTools::Rollback::Presenter, present: ['all good'])
      allow(ReleaseTools::Rollback::Presenter).to receive(:new).with(comparison, anything, { link_style: :markdown }).and_return(presenter)

      body = instance.to_issue_body

      expect(body).to match(/Rollback of `gprd` is available:/)
      expect(body).to match('all good')
    end

    it 'shows as unavailable' do
      instance = described_class.new
      comparison = comparison_stub(safe: false)
      allow(instance).to receive(:comparison).and_return(comparison)

      presenter = instance_spy(ReleaseTools::Rollback::Presenter, present: ['because reasons'])
      allow(ReleaseTools::Rollback::Presenter).to receive(:new).with(comparison, anything, { link_style: :markdown }).and_return(presenter)

      body = instance.to_issue_body

      expect(body).to match(/Rollback of `gprd` is unavailable:/)
      expect(body).to match('because reasons')
    end

    it 'gracefully handles a nil comparison' do
      stub_const('ReleaseTools::Rollback::CompareService', spy(execute: nil))

      instance = described_class.new

      expect(instance.to_issue_body).to eq(':warning: Unable to determine rollback availability.')
    end
  end
end
