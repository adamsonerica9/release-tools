# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Project::Quality::StagingCanary do
  it_behaves_like 'project .remotes'
  it_behaves_like 'project .to_s'

  describe '.path' do
    it { expect(described_class.path).to eq 'gitlab-org/quality/staging-canary' }
  end

  describe '.group' do
    it { expect(described_class.group).to eq 'gitlab-org/quality' }
  end

  describe '.ops_path' do
    it { expect(described_class.ops_path).to eq 'gitlab-org/quality/staging-canary' }
  end

  describe '.ops_group' do
    it { expect(described_class.ops_group).to eq 'gitlab-org/quality' }
  end

  describe '.requires_qa_issue?' do
    it { expect(described_class).not_to be_requires_qa_issue }
  end

  describe '.environment' do
    it { expect(described_class.environment).to eq('gstg-cny') }
  end
end
