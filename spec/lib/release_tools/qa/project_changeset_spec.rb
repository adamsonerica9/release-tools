# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Qa::ProjectChangeset, vcr: { cassette_name: 'commits-api' } do
  let(:project) { ReleaseTools::Project::GitlabEe }
  let(:from_ref) { 'v15.5.3-ee' }
  let(:to_ref) { 'v15.5.4-ee' }

  let(:expected_shas) do
    %w[f1db2eefe1d5039b220d4368dc25dbe5ad9060a1
       e3f0770dddda99530ebbfc8cf137aaa6d4b70ef9
       b450aae9dbc1a0cbe124de8e3643842a11a1ee77
       802d3cfe405be6d6917b4ddae53a59cd35c4ba90
       d0ee1589e00a695764a942e711dfe1be405f011c
       6c88e80d6c5380549997d04027dc8a72722cf4af
       ec2eb0164ea6e2bb3a6d3800e7d71f2f91017555
       8fddf02a2801d9f7f00b0a647e8a417bb648f4e9
       d3dda7548e0804cc53516a4493419f5616a8bcdf]
  end

  subject { described_class.new(project: project, from: from_ref, to: to_ref) }

  describe 'validations' do
    let(:from_ref) { 'invalid' }

    it 'raises an argument error for an invalid ref' do
      expect do
        subject
      end.to raise_error(ArgumentError)
    end
  end

  describe '#shas', vcr: { cassette_name: 'repository-api-compare' } do
    it 'has the correct shas' do
      expect(subject.shas).to eq(expected_shas)
    end
  end

  describe '#commits', vcr: { cassette_name: 'repository-api-compare' } do
    let(:shas) { subject.commits.pluck("id") }

    it 'downloads the list of commits from the API' do
      expect(shas).to eq(expected_shas)
    end
  end

  describe '#merge_requests', vcr: { cassette_name: %w[repository-api-compare merge-requests-api] } do
    let(:mr_ids) { subject.merge_requests.pluck("id") }

    it 'downloads the list of Merge Requests' do
      expect(mr_ids).to eq([188_060_105, 186_211_893, 188_281_010])
    end

    it 'retries retrieving of a merge request when this times out' do
      raised = false

      allow(ReleaseTools::GitlabClient).to receive(:merge_request) do
        if raised
          raised = true
          raise Errno::ETIMEDOUT
        else
          double(:merge_request)
        end
      end

      expect(subject.merge_requests.size).to be(3)
    end
  end
end
