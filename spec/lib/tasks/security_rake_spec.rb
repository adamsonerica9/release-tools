# frozen_string_literal: true

require 'rake_helper'

describe 'security tasks', :rake do
  after do
    # rake tasks in the security namespace run the force_security task altering
    # the global state
    ENV.delete('SECURITY')
  end

  describe 'sync_git_tags', task: 'security:sync_git_tags' do
    it 'syncs git tags' do
      expect(ReleaseTools::Security::SyncGitRemotesService).to receive(:new)
        .with(['1.0', '2.0', 'v3.1'])
        .and_return(instance_double(ReleaseTools::Security::SyncGitRemotesService, execute: true))

      subject.invoke('1.0 2.0 v3.1')
    end
  end

  describe 'prepare:disable_omnibus_nightly', task: 'security:prepare:disable_omnibus_nightly' do
    it 'disables omnibus nightly' do
      expect(ReleaseTools::Security::Prepare::OmnibusNightly).to receive(:new)
        .and_return(instance_double(ReleaseTools::Security::Prepare::OmnibusNightly, execute: true))

      subject.invoke
    end
  end

  describe 'process_security_target_issues', task: 'security:process_security_target_issues' do
    it 'runs the processor' do
      expect(ReleaseTools::Security::TargetIssuesProcessor).to receive(:new)
        .and_return(instance_double(ReleaseTools::Security::TargetIssuesProcessor, execute: true))

      subject.invoke
    end
  end
end
