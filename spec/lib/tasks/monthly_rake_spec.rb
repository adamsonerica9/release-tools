# frozen_string_literal: true

require 'rake_helper'

describe 'monthly tasks', :rake do
  describe 'finalize:start', task: 'monthly:finalize:start' do
    it 'starts the finalize stage' do
      expect(ReleaseTools::Slack::ReleasePipelineStartNotifier).to receive(:new).with(release_type: :monthly, stage: :finalize)
        .and_return(instance_double(ReleaseTools::Monthly::Finalize::UpdateProtectedBranches, execute: true))

      subject.invoke
    end
  end

  describe 'update_protected_branches', task: 'monthly:finalize:update_protected_branches' do
    it 'disables omnibus nightly' do
      expect(ReleaseTools::Monthly::Finalize::UpdateProtectedBranches).to receive(:new)
        .and_return(instance_double(ReleaseTools::Monthly::Finalize::UpdateProtectedBranches, execute: true))

      subject.invoke
    end
  end
end
