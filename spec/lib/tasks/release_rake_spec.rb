# frozen_string_literal: true

require 'rake_helper'

describe 'release tasks', :rake do
  describe 'issue', task: 'release:issue' do
    it_behaves_like 'handling invalid version args'

    it 'creates a release issue for the specified version' do
      expect(ReleaseTools::Tasks::Release::Issue).to receive(:new)
        .with('1.0.1')
        .and_return(instance_double(ReleaseTools::Tasks::Release::Issue, execute: true))

      subject.invoke('1.0.1')
    end
  end
end
