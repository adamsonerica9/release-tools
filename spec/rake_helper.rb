# frozen_string_literal: true

require 'spec_helper'
require 'rake'
require_relative '../lib/release_tools/tasks'

shared_context 'rake', :rake do
  let(:rake)      { Rake::Application.new }
  let(:task_name) { RSpec.current_example.metadata[:task] }
  let(:task_path) { "lib/tasks/#{task_name.split(':').first}" }

  subject { rake[task_name] }

  def loaded_files_excluding_current_rake_file
    $LOADED_FEATURES.reject { |file| file == "lib/tasks/#{task_path}.rake" }
  end

  before do
    Rake.application = rake
    Rake.application.rake_require(task_path, [Dir.pwd], loaded_files_excluding_current_rake_file)

    Rake::Task.define_task(:environment)
  end
end

RSpec.configure do
  include ReleaseTools::Tasks::Helper
end
