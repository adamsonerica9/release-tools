package handlers

import (
	"testing"

	"github.com/stretchr/testify/require"
)

type mockGauge struct {
	mockCounter
}

func (m *mockGauge) Set(value float64, labels ...string) {
	m.value = value
}

func (m *mockGauge) Dec(labels ...string) {
	m.value--
}

func TestGaugeIncHandler(t *testing.T) {
	meta := &mockGauge{}
	meta.expectedLabels = []string{"red"}
	handler := NewGauge(meta).(*gauge)

	req, err := meta.apiRequest("inc", "")
	require.NoError(t, err)

	meta.testRequest(t, req, handler, 1)

	resetReq, err := meta.apiReset()
	if err != nil {
		t.Fatal(err)
	}

	meta.testRequest(t, resetReq, handler, 0)
}

func TestGaugeSetHandler(t *testing.T) {
	meta := &mockGauge{}
	meta.expectedLabels = []string{"red", "high"}
	handler := NewGauge(meta).(*gauge)

	req, err := meta.apiRequest("set", "1.5")
	require.NoError(t, err)

	meta.testRequest(t, req, handler, 1.5)
}
