package main

import (
	"github.com/prometheus/client_golang/prometheus"

	"gitlab.com/gitlab-org/release-tools/metrics/internal/experiments"
	"gitlab.com/gitlab-org/release-tools/metrics/internal/handlers"
	"gitlab.com/gitlab-org/release-tools/metrics/internal/metrics"
	"gitlab.com/gitlab-org/release-tools/metrics/internal/metrics/labels"
)

// initMetrics initializes and resets all the metrics.
// Every metrics that requires an API handler must be returned in []handlers.Pluggable
//
// NOTE: the labels order is important! Pay attention to never change metric.WithLabel
// order when initializing a metric.
func initMetrics() ([]handlers.Pluggable, error) {
	_, err := metrics.NewCounterVec(
		metrics.WithName("info"),
		metrics.WithSubsystem("version"),
		metrics.WithHelp("Version info metadata"),
		metrics.WithLabel(labels.Nil("build_date")),
		metrics.WithLabel(labels.Nil("revision")),
		metrics.WithLabelReset(BuildDate, Revision))
	if err != nil {
		return nil, err
	}

	pluggables := make([]handlers.Pluggable, 0)
	err = initDeploymentMetrics(&pluggables)
	if err != nil {
		return pluggables, err
	}

	err = initPackagesMetrics(&pluggables)
	if err != nil {
		return pluggables, err
	}

	err = initExperimentsMetrics(&pluggables)
	if err != nil {
		return pluggables, err
	}

	err = initAutoDeployMetrics(&pluggables)
	if err != nil {
		return pluggables, err
	}

	err = initReleaseMetrics(&pluggables)
	if err != nil {
		return pluggables, err
	}

	return pluggables, nil
}

func initDeploymentMetrics(pluggables *[]handlers.Pluggable) error {
	subsystem := "deployment"
	deploymentLabelValues := map[string][]string{
		"deployment_type": {"coordinator_pipeline"},
		"target_env":      {"gstg-ref", "gstg-cny", "gstg", "gprd-cny", "gprd"},
	}

	durationHistogram, err := metrics.NewHistogramVec(
		metrics.WithName("duration_seconds"),
		metrics.WithSubsystem(subsystem),
		metrics.WithHelp("Duration of the coordinated deployment pipeline, from staging to production"),
		metrics.WithBuckets(prometheus.LinearBuckets(12_600, 30*60, 14)), // 14 buckets of 30 minutes ranging from 3.5hrs to 10h.
		metrics.WithLabel(labels.FromValues("deployment_type", deploymentLabelValues["deployment_type"])),
		metrics.WithLabel(labels.SuccessOrFailed("status")),
		metrics.WithCartesianProductLabelReset(),
	)
	if err != nil {
		return err
	}

	*pluggables = append(*pluggables, handlers.NewHistogram(durationHistogram))

	durationGauge, err := metrics.NewGaugeVec(
		metrics.WithName("duration_last_seconds"),
		metrics.WithSubsystem(subsystem),
		metrics.WithHelp("Duration of the last coordinated deployment pipeline, from staging to production"),
		metrics.WithLabel(labels.FromValues("deployment_type", deploymentLabelValues["deployment_type"])),
		metrics.WithLabel(labels.SuccessOrFailed("status")),
		metrics.WithLabel(labels.FullDeployVersion("deploy_version")),
	)
	if err != nil {
		return err
	}

	*pluggables = append(*pluggables, handlers.NewGauge(durationGauge))

	leadtimeGauge, err := metrics.NewGaugeVec(
		metrics.WithName("merge_request_lead_time_seconds"),
		metrics.WithSubsystem(subsystem),
		metrics.WithHelp("Time it takes from MR merge to production"),
		metrics.WithLabel(labels.Environment("target_env")),
		metrics.WithLabel(labels.Stage("target_stage")),
		metrics.WithLabel(labels.Integer("deployment_id")),
		metrics.WithLabel(labels.Integer("mr_id")),
		metrics.WithLabel(labels.FullDeployVersion("deploy_version")),
	)
	if err != nil {
		return err
	}
	*pluggables = append(*pluggables, handlers.NewGauge(leadtimeGauge))

	adjustedLeadtimeGauge, err := metrics.NewGaugeVec(
		metrics.WithName("merge_request_adjusted_lead_time_seconds"),
		metrics.WithSubsystem(subsystem),
		metrics.WithHelp("Time it takes from MR merge to production, adjusted to ignore weekends"),
		metrics.WithLabel(labels.Environment("target_env")),
		metrics.WithLabel(labels.Stage("target_stage")),
		metrics.WithLabel(labels.Integer("deployment_id")),
		metrics.WithLabel(labels.Integer("mr_id")),
		metrics.WithLabel(labels.FullDeployVersion("deploy_version")),
	)
	if err != nil {
		return err
	}
	*pluggables = append(*pluggables, handlers.NewGauge(adjustedLeadtimeGauge))

	envLabelValues := labels.Environment("").Values()
	stageLabelValues := labels.Stage("").Values()

	envLabelValuesWithEmpty := append(envLabelValues, "")
	stageLabelValuesWithEmpty := append(stageLabelValues, "")

	pipelineDurationGauge, err := metrics.NewGaugeVec(
		metrics.WithName("pipeline_duration_seconds"),
		metrics.WithSubsystem(subsystem),
		metrics.WithHelp("Wall clock duration of pipelines"),
		metrics.WithLabel(labels.AutoDeployProjects("project_name")),
		metrics.WithLabel(labels.FullDeployVersion("deploy_version")),
		metrics.WithLabel(labels.PipelineStatus("pipeline_status")),
		metrics.WithLabel(labels.AnyString("pipeline_name")),
		metrics.WithLabel(labels.Integer("pipeline_id")),
		metrics.WithLabel(labels.WebURL("web_url")), // Used for the dashboard links
		metrics.WithLabel(labels.FromValues("target_env", envLabelValuesWithEmpty)),
		metrics.WithLabel(labels.FromValues("target_stage", stageLabelValuesWithEmpty)),
		metrics.WithLabel(labels.AnyString("upstream_pipeline_name")),
	)
	if err != nil {
		return err
	}
	*pluggables = append(*pluggables, handlers.NewGauge(pipelineDurationGauge))

	jobDurationGauge, err := metrics.NewGaugeVec(
		metrics.WithName("job_duration_seconds"),
		metrics.WithSubsystem(subsystem),
		metrics.WithHelp("Wall clock duration of jobs"),
		metrics.WithLabel(labels.AnyString("job_name")),
		metrics.WithLabel(labels.AnyString("job_stage")),
		metrics.WithLabel(labels.SuccessOrFailed("job_status")),
		metrics.WithLabel(labels.AutoDeployProjects("project_name")),
		metrics.WithLabel(labels.FullDeployVersion("deploy_version")),
		metrics.WithLabel(labels.FromValues("target_env", envLabelValuesWithEmpty)),
		metrics.WithLabel(labels.FromValues("target_stage", stageLabelValuesWithEmpty)),
		metrics.WithLabel(labels.AnyString("short_job_name")),
		metrics.WithLabel(labels.WebURL("web_url")), // Used for the dashboard links
		metrics.WithLabel(labels.Integer("job_id")),
		metrics.WithLabel(labels.Integer("pipeline_id")),
		metrics.WithLabel(labels.AnyString("pipeline_name")),
	)
	if err != nil {
		return err
	}
	*pluggables = append(*pluggables, handlers.NewGauge(jobDurationGauge))

	deploymentStartedCounter, err := metrics.NewCounterVec(
		metrics.WithName("started_total"),
		metrics.WithSubsystem(subsystem),
		metrics.WithHelp("Number of deployments started for each environment"),
		metrics.WithLabel(labels.FromValues("target_env", deploymentLabelValues["target_env"])),
		metrics.WithCartesianProductLabelReset(),
	)
	if err != nil {
		return err
	}

	*pluggables = append(*pluggables, handlers.NewCounter(deploymentStartedCounter))

	deploymentCanRollbackCounter, err := metrics.NewCounterVec(
		metrics.WithName("can_rollback_total"),
		metrics.WithSubsystem(subsystem),
		metrics.WithHelp("Number of deployments suitable for rollback for each environment"),
		metrics.WithLabel(labels.FromValues("target_env", deploymentLabelValues["target_env"])),
		metrics.WithCartesianProductLabelReset(),
	)
	if err != nil {
		return err
	}

	*pluggables = append(*pluggables, handlers.NewCounter(deploymentCanRollbackCounter))

	rollbacksCounter, err := metrics.NewCounterVec(
		metrics.WithName("rollbacks_started_total"),
		metrics.WithSubsystem(subsystem),
		metrics.WithHelp("Number of rollbacks started for each environment"),
		metrics.WithLabel(labels.FromValues("target_env", deploymentLabelValues["target_env"])),
		metrics.WithCartesianProductLabelReset(),
	)
	if err != nil {
		return err
	}

	*pluggables = append(*pluggables, handlers.NewCounter(rollbacksCounter))

	deploymentCompletedCounter, err := metrics.NewCounterVec(
		metrics.WithName("completed_total"),
		metrics.WithSubsystem(subsystem),
		metrics.WithHelp("Number of deployments completed for each environment"),
		metrics.WithLabel(labels.FromValues("target_env", deploymentLabelValues["target_env"])),
		metrics.WithCartesianProductLabelReset(),
	)
	if err != nil {
		return err
	}

	*pluggables = append(*pluggables, handlers.NewCounter(deploymentCompletedCounter))

	deploymentStartedGauge, err := metrics.NewGaugeVec(
		metrics.WithName("started"),
		metrics.WithSubsystem(subsystem),
		metrics.WithHelp("Number of deployments started for each environment as gauge"),
		metrics.WithLabel(labels.FromValues("target_env", deploymentLabelValues["target_env"])),
		metrics.WithLabel(labels.FullDeployVersion("version")),
	)
	if err != nil {
		return err
	}

	*pluggables = append(*pluggables, handlers.NewGauge(deploymentStartedGauge))

	deploymentCompletedGauge, err := metrics.NewGaugeVec(
		metrics.WithName("completed"),
		metrics.WithSubsystem(subsystem),
		metrics.WithHelp("Number of deployments completed for each environment as gauge"),
		metrics.WithLabel(labels.FromValues("target_env", deploymentLabelValues["target_env"])),
		metrics.WithLabel(labels.FullDeployVersion("version")),
	)
	if err != nil {
		return err
	}

	*pluggables = append(*pluggables, handlers.NewGauge(deploymentCompletedGauge))

	deploymentFailedAtLeastOnceGauge, err := metrics.NewGaugeVec(
		metrics.WithName("failed_atleast_once"),
		metrics.WithSubsystem(subsystem),
		metrics.WithHelp("A deployment with at least one failed job"),
		metrics.WithLabel(labels.FullDeployVersion("deploy_version")),
	)
	if err != nil {
		return err
	}

	*pluggables = append(*pluggables, handlers.NewGauge(deploymentFailedAtLeastOnceGauge))

	return nil
}

func initPackagesMetrics(pluggables *[]handlers.Pluggable) error {
	subsystem := "packages"

	//NOTE (nolith): we should consider tracking also RCs and public packages
	pkgTypes := []string{"auto_deploy", "monthly", "patch", "rc", "security"}
	securityTypes := []string{"no", "regular", "critical"}

	taggingCounter, err := metrics.NewCounterVec(
		metrics.WithName("tagging_total"),
		metrics.WithSubsystem(subsystem),
		metrics.WithHelp("Number of tagged packages by package type"),
		metrics.WithLabel(labels.FromValues("pkg_type", pkgTypes)),
		metrics.WithLabel(labels.FromValues("security", securityTypes)),
		metrics.WithCartesianProductLabelReset(),
	)
	if err != nil {
		return err
	}

	*pluggables = append(*pluggables, handlers.NewCounter(taggingCounter))

	return nil
}

func initExperimentsMetrics(pluggables *[]handlers.Pluggable) error {
	experimentHandlers, err := experiments.GetHandlers()
	if err != nil {
		return err
	}

	*pluggables = append(*pluggables, experimentHandlers...)

	return nil
}

func initAutoDeployMetrics(pluggables *[]handlers.Pluggable) error {
	subsystem := "auto_deploy"

	roles := []string{"gstg-cny", "gprd-cny", "gstg", "gprd"}

	pressureGauge, err := metrics.NewGaugeVec(
		metrics.WithName("pressure"),
		metrics.WithSubsystem(subsystem),
		metrics.WithHelp("Number of commits not yet deployed to an environment"),
		metrics.WithLabel(labels.FromValues("role", roles)),
		metrics.WithCartesianProductLabelReset(),
	)
	if err != nil {
		return err
	}

	*pluggables = append(*pluggables, handlers.NewGauge(pressureGauge))

	pickCounter, err := metrics.NewCounterVec(
		metrics.WithName("picks_total"),
		metrics.WithSubsystem(subsystem),
		metrics.WithHelp("Number of merge requests picked into auto-deploy branch"),
		metrics.WithLabel(labels.SuccessOrFailed("status")),
		metrics.WithCartesianProductLabelReset(),
	)
	if err != nil {
		return err
	}

	*pluggables = append(*pluggables, handlers.NewCounter(pickCounter))

	pipelineStatusCounter, err := metrics.NewCounterVec(
		metrics.WithName("gitlab_pipeline_total"),
		metrics.WithSubsystem(subsystem),
		metrics.WithHelp("Number of gitlab pipelines in a given status at the time of rollout"),
		metrics.WithLabel(labels.PipelineStatus("status")),
		metrics.WithCartesianProductLabelReset(),
	)
	if err != nil {
		return err
	}

	*pluggables = append(*pluggables, handlers.NewCounter(pipelineStatusCounter))

	return nil
}

func initReleaseMetrics(pluggables *[]handlers.Pluggable) error {
	subsystem := "release"

	severities := []string{"severity::1", "severity::2", "severity::3", "severity::4", "none"}

	pressureGauge, err := metrics.NewGaugeVec(
		metrics.WithName("pressure"),
		metrics.WithSubsystem(subsystem),
		metrics.WithHelp("Number of unreleased merge requests"),
		metrics.WithLabel(labels.FromValues("severity", severities)),
		metrics.WithLabel(labels.MinorVersion("version")),
	)
	if err != nil {
		return err
	}

	*pluggables = append(*pluggables, handlers.NewGauge(pressureGauge))

	return nil
}
