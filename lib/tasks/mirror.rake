# frozen_string_literal: true

namespace :mirror do
  desc 'Checks the security mirror status'
  task :status do
    ReleaseTools::Security::SecurityMirrorNotificationService.new.execute
  end
end
