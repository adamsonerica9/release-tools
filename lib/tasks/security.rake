namespace :security do
  # Undocumented; should be a pre-requisite for every task in this namespace!
  task :force_security do
    unless ReleaseTools::SharedStatus.critical_security_release?
      ENV['SECURITY'] = 'true'
    end
  end

  desc 'Create a security release task issue'
  task issue: :force_security do |_t, args|
    issue = ReleaseTools::PatchRelease::SecurityIssue.new(versions: args[:versions])

    create_or_show_issue(issue)
  end

  desc 'Merges valid security merge requests'
  task :merge, [:merge_default] => :force_security do |_t, args|
    merge_default =
      if args[:merge_default] && !args[:merge_default].empty?
        true
      else
        false
      end

    ReleaseTools::Security::MergeRequestsMerger
      .new(merge_default: merge_default)
      .execute
  end

  desc 'Toggle the security merge train based on need'
  task :merge_train do |_t, _args|
    ReleaseTools::Security::MergeTrainService
      .new
      .execute
  end

  desc 'Prepare for a new security release'
  task prepare: :force_security do |_t, _args|
    issue_task = Rake::Task['security:issue']
    issue_task.execute
  end

  desc 'Publish the current security release'
  task :publish, [:version] => :force_security do |_t, args|
    $stdout
      .puts "Security Release - using security repository only!\n"
      .colorize(:red)

    publish_task = Rake::Task['publish']
    publish_task.execute(version: args[:version])
  end

  desc "Check a security release's build status"
  task status: :force_security do |t, _args|
    status = ReleaseTools::BranchStatus.for_security_release

    status.each_pair do |project, results|
      results.each do |result|
        ReleaseTools.logger.tagged(t.name) do
          ReleaseTools.logger.info(project, result.to_h)
        end
      end
    end

    ReleaseTools::Slack::ChatopsNotification.branch_status(status)
  end

  desc 'Tag a new security release'
  task :tag, [:version] => :force_security do |_t, args|
    $stdout
      .puts "Security Release - using security repository only!\n"
      .colorize(:red)

    Rake::Task['release:tag'].invoke(*args)
  end

  desc 'Validates merge requests in security projects'
  task validate: :force_security do
    ReleaseTools::Security::ProjectsValidator
      .new(ReleaseTools::Security::Client.new)
      .execute
  end

  desc 'Sync default and auto-deploy branches'
  task sync_remotes: :force_security do
    ReleaseTools::Security::SyncRemotesService
      .new
      .execute
  end

  desc 'Sync Git tags'
  task :sync_git_tags, [:git_versions] => :force_security do |_t, args|
    git_versions = args[:git_versions].split

    ReleaseTools::Security::SyncGitRemotesService
      .new(git_versions)
      .execute
  end

  namespace :gitaly do
    desc 'Tag a new Gitaly security release'
    task :tag, [:version] => :force_security do |_, args|
      Rake::Task['release:gitaly:tag'].invoke(*args)
    end
  end

  desc 'Link/Unlink security-target issues for a security release'
  task process_security_target_issues: :force_security do
    ReleaseTools::Security::TargetIssuesProcessor.new.execute
  end

  namespace :prepare do
    desc 'Notify the prepare pipeline has started'
    task start: :force_security do
      ReleaseTools::Slack::ReleasePipelineStartNotifier
        .new(stage: :start, release_type: :security)
        .execute
    end

    desc 'Disable Omnibus Nightly builds'
    task disable_omnibus_nightly: :force_security do
      ReleaseTools::Security::Prepare::OmnibusNightly
        .new(action: :disable)
        .execute
    end

    desc 'Notify JiHu about the security release'
    task notify_jihu: :force_security do
      ReleaseTools::Security::Prepare::NotifyJihu.new.execute
    end

    desc 'Notify a stage team about the security release'
    task notify_stage_team: :force_security do
      team = ENV.fetch('STAGE_TEAM', nil).to_sym

      ReleaseTools::Slack::Security::TeamNotifier
        .new(team: team)
        .execute
    end

    desc 'Check components for green pipelines'
    task check_component_branch_pipeline_status: :force_security do
      ReleaseTools::Security::Prepare::ComponentBranchVerifier.new.execute
    end
  end

  namespace :finalize do
    desc 'Notify the finalize pipeline has started'
    task start: :force_security do
      ReleaseTools::Slack::ReleasePipelineStartNotifier
        .new(stage: :finalize, release_type: :security)
        .execute
    end

    desc 'Close implementation issues'
    task close_issues: :force_security do
      ReleaseTools::Security::Finalize::CloseImplementationIssues
        .new
        .execute
    end

    desc 'Enable Omnibus Nightly builds'
    task enable_omnibus_nightly: :force_security do
      ReleaseTools::Security::Prepare::OmnibusNightly
        .new(action: :enable)
        .execute
    end

    desc 'Notify the completion of the security release'
    task notify_release: :force_security do
      ReleaseTools::Security::Finalize::NotifyReleaseComplete.new.execute
    end

    desc 'Enable Gitaly Update Task'
    task enable_gitaly_update_task: :force_security do
      ReleaseTools::Security::Finalize::GitalyUpdateTask
        .new(action: :enable)
        .execute
    end

    desc 'Check tags are synced to canonical'
    task check_canonical_tags_synced: :force_security do
      ReleaseTools::Security::Finalize::CheckCanonicalTagsSynced.new.execute
    end

    desc 'Update Security Release Tracking Issue'
    task update_tracking_issue: :force_security do
      updater = ReleaseTools::Security::Finalize::CloseTrackingIssue.new
      updater.execute

      security_tracking_issue = ReleaseTools::Security::TrackingIssue.new
      create_or_show_issue(security_tracking_issue)
    end

    desc 'Notify upcoming release managers'
    task notify_upcoming_release_managers: :force_security do
      ReleaseTools::Security::Finalize::NotifyNextReleaseManagers.new.execute
    end

    desc 'Update the slack bookmark of the security tracking issue'
    task update_slack_bookmark: :force_security do
      ReleaseTools::Security::Finalize::UpdateSlackBookmark
        .new.execute
    end
  end
end
