# frozen_string_literal: true

namespace :monthly do
  namespace :finalize do
    desc 'Notify the finalize pipeline has started'
    task :start do
      ReleaseTools::Slack::ReleasePipelineStartNotifier
        .new(stage: :finalize, release_type: :monthly)
        .execute
    end

    desc 'Update the protected stable branches'
    task :update_protected_branches do
      ReleaseTools::Monthly::Finalize::UpdateProtectedBranches.new.execute
    end
  end
end
