# frozen_string_literal: true

desc 'Execute all metrics tasks'
task :metrics do
  tasks = %w[
    auto_deploy_pressure
    mirror_status
    patch_release_pressure
  ]

  errors = []

  Parallel.each(tasks, in_threads: Etc.nprocessors) do |task|
    Rake::Task["metrics:#{task}"].invoke
  rescue StandardError => ex
    ReleaseTools.logger.warn("Error updating metric", { task: task }, ex)

    errors << ex
  end

  abort if errors.any?
end

namespace :metrics do
  desc "Update auto-deploy pressure metric"
  task :auto_deploy_pressure do
    ReleaseTools::Metrics::AutoDeployPressure.new.execute
  end

  desc "Update mirror status metric"
  task :mirror_status do
    ReleaseTools::Metrics::MirrorStatus.new.execute
  end

  desc "Increment deployment_started_total for $DEPLOY_ENVIRONMENT environment"
  task :deployment_started do
    ReleaseTools::Tasks::Metrics::TrackDeploymentStarted.new.execute
  end

  desc "Increment deployment_failed_atleast_once for $DEPLOY_VERSION deploy version"
  task :deployment_failed_atleast_once do
    ReleaseTools::Tasks::Metrics::TrackDeploymentFailedAtleastOnce.new.execute
  end

  desc 'Track deployment metrics'
  task :deployment_metrics, [:environment, :version] do |_, args|
    env = args[:environment]
    version = args[:version]
    ReleaseTools::Tasks::Metrics::DeploymentMetrics::MergeRequestLeadTime.new.execute(env, version)
  end

  desc "Increment deployment_completed_total for $DEPLOY_ENVIRONMENT environment"
  task :deployment_completed do
    ReleaseTools::Tasks::Metrics::TrackDeploymentCompleted.new.execute
  end

  desc "Update patch release pressure metric"
  task :patch_release_pressure do
    ReleaseTools::Metrics::PatchReleasePressure.new.execute
  end

  desc "Update pipeline and job duration metrics"
  task :pipeline_and_job_duration, [:pipeline_url, :deploy_version] do |_, args|
    pipeline_url = args[:pipeline_url]
    deploy_version = args[:deploy_version]

    ReleaseTools::PipelineTracer::MetricsService
      .from_pipeline_url(pipeline_url, version: deploy_version, depth: 3)
      .execute
  end
end
