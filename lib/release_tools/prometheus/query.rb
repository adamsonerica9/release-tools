# frozen_string_literal: true

module ReleaseTools
  module Prometheus
    class Query
      include ::SemanticLogger::Loggable

      class PromQLError < StandardError
        attr_reader :status_code, :body

        def initialize(status_code, body)
          @status_code = status_code
          @body = body
        end

        def to_s
          "API failure. Status code: #{status_code}, body: #{body}"
        end
      end

      ENDPOINT = "http://#{ENV.fetch('PROMETHEUS_HOST', nil)}/api/v1/query".freeze

      # NOTE: Use Gitaly nodes since everything else is deployed via K8S
      # See https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/2169
      # rubocop:disable Layout/HashAlignment
      ROLE_VERSIONS = {
        'gprd'     => 'component="gitlab-rails", type="gitaly", env="gprd", stage="main"',
        'gprd-cny' => 'component="gitlab-rails", type="gitaly", env="gprd", stage="cny"',
        'gstg'     => 'component="gitlab-rails", type="gitaly", env="gstg", stage="main"',
        'gstg-cny' => 'component="gitlab-rails", type="gitaly", env="gstg", stage="cny"',
        'gstg-ref' => 'component="gitlab-rails", type="gitaly", env="gstg", stage="ref"'
      }.freeze
      # rubocop:enable Layout/HashAlignment

      def run(query)
        logger.trace('PromQL query', query: query)

        response = Retriable.retriable { get(query) }

        response.parse
      end

      def rails_version_by_role
        return {} unless ENV['PROMETHEUS_HOST']

        ROLE_VERSIONS.each_with_object({}) do |(role, params), memo|
          query = "topk(1, count(gitlab_version_info{#{params}}) by (version))"
          begin
            response = run(query)

            # jq '.data .result | .[] | .metric'
            metric = response.dig('data', 'result', 0, 'metric')
            next memo[role] = '' if metric.nil?

            memo[role] = metric['version']
          rescue PromQLError
            next memo[role] = ''
          end
        end
      end

      private

      def get(query)
        response = HTTP.get(ENDPOINT, params: { query: query })

        unless response.status.success?
          error = PromQLError.new(response.status.code, response.body.to_s)
          logger.warn('API Failure', status: error.status_code.to_s, error: error.body)

          raise error
        end

        response
      end
    end
  end
end
