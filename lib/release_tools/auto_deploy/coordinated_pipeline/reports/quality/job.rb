# frozen_string_literal: true

module ReleaseTools
  module AutoDeploy
    module CoordinatedPipeline
      module Reports
        module Quality
          class Job
            include Helper

            SPEC_MATCHER = /qa\/specs\/features\/(.*)_spec.rb/

            def initialize(data, environment, pipeline_type)
              @data = data
              @environment = environment
              @pipeline_type = pipeline_type
            end

            def web_url
              data.web_url
            end

            def errors
              trace
                .split('Failures:')
                .last
                .split('Randomized with seed')
                .first
                .strip
            end

            def title
              errors.match(SPEC_MATCHER).to_s
            end

            private

            attr_reader :data, :environment, :pipeline_type

            def trace
              @trace ||= client.job_trace(
                project,
                data.id
              )
            end
          end
        end
      end
    end
  end
end
