# frozen_string_literal: true

module ReleaseTools
  module AutoDeploy
    module CoordinatedPipeline
      module Reports
        module Helper
          QUALITY_PROJECTS = {
            'qa:smoke:gstg-cny' => ReleaseTools::Project::Quality::StagingCanary,
            'qa:smoke-main:gstg-cny' => ReleaseTools::Project::Quality::Staging,
            'qa:smoke:gprd-cny' => ReleaseTools::Project::Quality::Canary,
            'qa:smoke-main:gprd' => ReleaseTools::Project::Quality::Production
          }.freeze

          private

          def client
            ReleaseTools::GitlabOpsClient
          end

          def project
            QUALITY_PROJECTS[pipeline_type]
          end
        end
      end
    end
  end
end
