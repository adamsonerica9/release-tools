# frozen_string_literal: true

module ReleaseTools
  module Services
    # AddToMergeTrainService adds a merge request to the Merge Train
    class AddToMergeTrainService
      MergeTrainsNotEnabledError = Class.new(StandardError)

      attr_reader :merge_request

      # Initialize the service
      #
      # @param merge_request [ReleaseTools::MergeRequest] the merge request to approve and MWPS
      # @param token [string] the GitLab private token, it should not belong to the merge request's author
      def initialize(merge_request, token:)
        @merge_request = merge_request
        @token = token
      end

      def execute(sha)
        Retriable.with_context(:api) do
          validate!

          Services::ApproveService.new(merge_request, token: @token).execute

          add_to_merge_train(sha)
        end
      end

      def add_to_merge_train(sha)
        GitlabClient.add_to_merge_train(merge_request, sha, token: @token)
      end

      # Validates the merge request exists and that Merge Trains are enabled for the project.
      #
      # @raise [MergeTrainsNotEnabledError] when Merge Trains are not enabled on the MR project
      def validate!
        raise MergeTrainsNotEnabledError unless GitlabClient.merge_trains_enabled?(merge_request.project)
      end
    end
  end
end
