# frozen_string_literal: true

module ReleaseTools
  class MonthlyIssue < Issue
    def create
      monthly_release_pipeline if monthly_release_pipeline?

      super
    end

    def title
      "Release #{version.to_minor}"
    end

    def labels
      'Monthly Release,team::Delivery'
    end

    def project
      ::ReleaseTools::Project::Release::Tasks
    end

    def assignees
      ReleaseManagers::Schedule.new.active_release_managers.collect(&:id)
    rescue ReleaseManagers::Schedule::VersionNotFoundError
      nil
    end

    def current_stable_branch
      ReleaseTools::Versions.current_stable_branch
    end

    def monthly_release_pipeline?
      Feature.enabled?(:monthly_release_pipeline)
    end

    def monthly_release_pipeline
      @monthly_release_pipeline ||= GitlabOpsClient.create_pipeline(
        Project::ReleaseTools,
        MONTHLY_RELEASE_PIPELINE: 'true'
      )
    end

    def release_date
      @release_date ||= Date.parse(
        ReleaseTools::GitlabReleasesClient.upcoming_releases[version.to_minor]
      )
    end

    def formated_date(date)
      date.strftime("%A, %b %-d")
    end

    def preparation_start_day
      if dynamic_release_date?
        formated_date(release_date - 6.days)
      else
        <<~STR
          17th

          If this date is on a weekend, do this work on the next working day
        STR
      end
    end

    def candidate_selection_day
      if dynamic_release_date?
        formated_date(release_date - 3.days)
      else
        <<~STR
          18th

          If this date is on a weekend, do this work on the last Friday before the 18th.
        STR
      end
    end

    def rc_tag_day
      if dynamic_release_date?
        formated_date(release_date - 2.days)
      else
        <<~STR
          20th - two working days before the release

          If this date is on a Sunday, do this work on the last Friday before the 20th.
          If it falls on a Friday or Saturday, move it to Thursday.
        STR
      end
    end

    def tag_day
      if dynamic_release_date?
        formated_date(release_date - 1.day)
      else
        <<~STR
          21st - one day before the release

          If this date is on a weekend, do this work on the Friday before that weekend.
        STR
      end
    end

    def release_day
      if dynamic_release_date?
        formated_date(release_date)
      else
        '22nd'
      end
    end

    def ordinalized_release_date
      return '22nd' unless dynamic_release_date?

      release_date.day.ordinalize
    end

    def dynamic_release_date?
      Feature.enabled?(:dynamic_release_date)
    end

    protected

    def template_path
      File.expand_path('../../templates/monthly.md.erb', __dir__)
    end
  end
end
