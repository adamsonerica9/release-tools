# frozen_string_literal: true

module ReleaseTools
  module PatchRelease
    class BlogMergeRequest < MergeRequest
      include ::SemanticLogger::Loggable

      def labels
        'patch release post'
      end

      def title
        "Draft: Adding #{version_str} blog post"
      end

      def assignee_ids
        release_managers&.collect(&:id)
      end

      def source_branch
        "create-#{hyphenated_version}-post"
      end

      def create
        return unless create_blog_post_file

        super
      end

      def patch_issue_url
        if dry_run?
          'test.gitlab.com'
        else
          patch_issue.url
        end
      end

      def generate_blog_content
        raise 'content is not provided' unless content

        template_text = File.read('templates/patch_release_blog_post_template.html.md.erb')

        ERB.new(template_text, trim_mode: '-').result(binding)
      end

      def blog_post_filename
        "#{Date.current}-gitlab-#{hyphenated_version}-released.html.md"
      end

      protected

      def hyphenated_version
        versions_str.tr(', ', '--').tr('.', '-')
      end

      def template_path
        File.expand_path('../../../templates/patch_blog_post_merge_request_description.md.erb', __dir__)
      end

      def create_blog_post_file
        file_content = generate_blog_content

        filepath = blog_post_filepath + blog_post_filename

        logger.info('Created blog post file')

        Retriable.with_context(:api) do
          GitlabClient.find_or_create_branch(source_branch, target_branch, project)
        end

        logger.info('Created branch', branch_name: source_branch)

        commit(file_content, filepath)
      end

      def commit(file_content, filepath, action = 'create', message = nil)
        message ||= "Adding #{version_str} blog post"

        actions = [{
          action: action,
          file_path: filepath,
          content: file_content
        }]

        Retriable.with_context(:api) do
          GitlabClient.create_commit(
            project.path,
            source_branch,
            message,
            actions
          )
        end
      end

      def blog_post_filepath
        "sites/uncategorized/source/releases/posts/"
      end

      def patch_issue
        @patch_issue ||= Issue.new
      end

      def release_managers
        ReleaseManagers::Schedule.new.active_release_managers
      rescue ReleaseManagers::Schedule::VersionNotFoundError
        logger.fatal('Could not find active release managers')
        nil
      end

      # Method adjusted until the maintenance policy is extended
      # https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/971
      def version_str
        versions_str.to_patch
      end

      # Method adjusted until used when the maintenance policy is extended
      # https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/971
      def versions_str
        content.first[:version]
      end

      # Method adjusted until used when the maintenance policy is extended
      # https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/971
      def releases
        [content.first]
      end
    end
  end
end
