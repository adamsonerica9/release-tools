# frozen_string_literal: true

require 'slack-ruby-block-kit'

require 'release_tools/slack/bookmark'
require 'release_tools/slack/webhook'
require 'release_tools/slack/utilities'
require 'release_tools/slack/auto_deploy_notification'
require 'release_tools/slack/coordinated_pipeline_diffs_notification'
require 'release_tools/slack/coordinated_pipeline_notification'
require 'release_tools/slack/coordinated_pipeline_tag_notification'
require 'release_tools/slack/chatops_notification'
require 'release_tools/slack/merge_train_notification'
require 'release_tools/slack/message'
require 'release_tools/slack/patch_release_merge_requests_notification'
require 'release_tools/slack/post_deploy_migrations_notification'
require 'release_tools/slack/post_deploy_pipeline_initial_notification'
require 'release_tools/slack/qa_notification'
require 'release_tools/slack/release_pipeline_helpers'
require 'release_tools/slack/release_pipeline_start_notifier'
require 'release_tools/slack/release_job_end_notifier'
require 'release_tools/slack/search'
require 'release_tools/slack/security/mirror_message'
require 'release_tools/slack/security/team_notifier'
require 'release_tools/slack/tag_notification'

module ReleaseTools
  module Slack
    ANNOUNCEMENTS = 'C8PKBH3M5'
    F_UPCOMING_RELEASE = 'C0139MAV672'
    G_ENGINEERING_PRODUCTIVITY = 'CMA7DQJRX'
    G_RUNNER = 'CBQ76ND6W'
    GITALY_ALERTS = 'C4MU5R2MD'
    NOTIFICATION_TESTS = 'C047XHV7PQQ'
    RELEASE_MANAGERS = 'S0127FU8PDE'
    RELEASES = 'C0XM5UU6B'
    QUALITY = 'C3JJET4Q6'
    TEAM = 'T02592416'

    ANNOUNCEMENTS_NAME = 'announcements'
  end
end
