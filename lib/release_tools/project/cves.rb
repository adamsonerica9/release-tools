# frozen_string_literal: true

module ReleaseTools
  module Project
    class Cves < BaseProject
      REMOTES = {
        canonical: 'git@gitlab.com:gitlab-org/cves.git'
      }.freeze

      IDS = {
        canonical: 18_741_849
      }.freeze
    end
  end
end
