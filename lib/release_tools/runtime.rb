# frozen_string_literal: true

module ReleaseTools
  module Runtime
    SERVERS = {
      'https://ops.gitlab.net' => :ops,
      'https://gitlab.com' => :production,
      'https://dev.gitlab.org' => :dev
    }.freeze

    def self.current_environment
      SERVERS[ci_server_url]
    end

    def self.ci_server_url
      ENV.fetch("CI_SERVER_URL", nil)
    end

    def self.valid_environment?
      current_environment.present?
    end
  end
end
