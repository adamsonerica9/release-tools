# frozen_string_literal: true

require 'date'

module ReleaseTools
  module Security
    class TrackingIssue < ReleaseTools::Issue
      include ::SemanticLogger::Loggable

      DEFAULT_DUE_DATE_DAY = 28

      def title
        prefix =
          if SharedStatus.critical_security_release?
            'Critical security'
          else
            'Security'
          end

        "#{prefix} release: #{versions_title}"
      end

      def confidential?
        true
      end

      def labels
        'upcoming security release,security,meta'
      end

      def project
        ReleaseTools::Project::GitlabEe
      end

      def versions_title
        versions = [
          next_version,
          ReleaseTools::Versions.next_versions.take(2)
        ].flatten

        versions.map! do |version|
          version.sub(/\d+$/, 'x')
        end.join(', ')
      end

      # Regular Security Releases are performed in the next milestone of the
      # last version released, e.g if the last version released is 13.6, the
      # Security Release will be performed on %13.7.
      def version
        next_version
      end

      def due_date
        return if Feature.enabled?(:dynamic_release_date)

        current_date = Date.today

        # due_date_month depends on the date the security release was completed,
        # this one can be wrapped up on the 28th, but it can also be completed
        # the first week of the next month, e.g. if it's completed on Nov 28th
        # the next due date will be Dec 28th, but if it's completed on Dec 2nd,
        # the due date still needs to be Dec 28th.
        due_date_month = if current_date.day < 10
                           current_date.month
                         else
                           current_date.next_month.month
                         end

        Date.new(current_date.year, due_date_month, DEFAULT_DUE_DATE_DAY).to_s
      end

      def assignees
        ReleaseManagers::Schedule.new.active_release_managers.collect(&:id)
      rescue ReleaseManagers::Schedule::VersionNotFoundError
        nil
      end

      protected

      def template_path
        File.expand_path('../../../templates/security_release_tracking_issue.md.erb', __dir__)
      end

      def next_version
        @next_version ||= ReleaseTools::Version.new(ReleaseTools::ProductMilestone.next.title)
      end
    end
  end
end
