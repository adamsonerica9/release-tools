# frozen_string_literal: true

module ReleaseTools
  module Security
    # Fetches issues associated to the Security Release Tracking Issue
    # that are ready to be processed
    class IssuesFetcher
      include ::SemanticLogger::Loggable

      def initialize(client)
        @client = client
        @ready = []
        @pending = []
      end

      def execute
        return [] if security_issues.empty?

        logger.info("#{security_issues.count} associated to the Security Release Tracking issue.")

        @ready, @pending = security_issues
          .reject(&:processed?)
          .partition(&:ready_to_be_processed?)

        log_result

        @ready
      end

      private

      def security_issues
        @security_issues ||=
          Security::IssueCrawler
            .new
            .upcoming_security_issues_and_merge_requests
      end

      def log_result
        return if @pending.empty?

        @pending.each do |issue|
          logger.warn("Unable to process security issue", url: issue.web_url, pending_reasons: issue.pending_reasons.join(', '))
        end
      end
    end
  end
end
