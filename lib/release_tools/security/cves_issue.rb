# frozen_string_literal: true

module ReleaseTools
  module Security
    class CvesIssue
      include ::SemanticLogger::Loggable

      YamlError = Class.new(StandardError)
      YamlMissingError = Class.new(YamlError)
      YamlInvalidError = Class.new(YamlError)
      YamlParseError = Class.new(YamlError)

      delegate :title, to: :issue

      # @param [Gitlab::ObjectifiedHash] issue is an object from the response of the issues list API
      def initialize(issue)
        @issue = issue
      end

      def cvss_string
        yaml.dig('vulnerability', 'impact')
      end

      def vulnerability_description
        yaml.dig('vulnerability', 'description')
      end

      def credit
        yaml.dig('vulnerability', 'credit')
      end

      def yaml
        return @yaml if defined?(@yaml)

        yaml_str = issue.description.gsub(/^.*```yaml\n|\n```.*$/m, '')

        unless yaml_str.present?
          logger.error('CVE issue does not have YAML', issue: issue.web_url)
          raise YamlMissingError, 'CVE issue does not have YAML'
        end

        @yaml = load_yaml(yaml_str)

        unless @yaml.is_a?(Hash)
          logger.error('CVE issue YAML is not a Hash', issue: issue.web_url, yaml_str: yaml_str)
          raise YamlInvalidError, 'CVE issue YAML is not a Hash'
        end

        @yaml
      end

      private

      attr_reader :issue

      def load_yaml(yaml_str)
        YAML.safe_load(yaml_str)
      rescue Psych::DisallowedClass, Psych::BadAlias, Psych::SyntaxError => ex
        logger.error('CVE issue contains invalid YAML', issue: issue.web_url, yaml_str: yaml_str, error: ex.inspect)

        raise YamlParseError, 'CVE issue contains invalid YAML'
      end
    end
  end
end
