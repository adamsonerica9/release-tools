# frozen_string_literal: true

module ReleaseTools
  module Security
    module Finalize
      class NotifyReleaseComplete
        include ::SemanticLogger::Loggable

        CouldNotCompleteError = Class.new(StandardError)

        def initialize
          @versions = ReleaseTools::Versions.next_versions.map(&:previous_patch)
        end

        def execute
          logger.info('Notifying security release completion', versions: versions)

          @blog_post_url = BlogPostValidator
            .new(version: versions.first)
            .execute

          trigger_pipeline_on_chatops
          send_slack_notification(:success)
        rescue StandardError => ex
          logger.fatal(error_message, error: ex)
          send_slack_notification(:failed)

          raise CouldNotCompleteError
        end

        private

        attr_reader :versions, :blog_post_url

        def trigger_pipeline_on_chatops
          pipeline = GitlabOpsClient.create_pipeline(
            ReleaseTools::Project::ChatOps,
            TRIGGER_NOTIFY: true,
            CHAT_INPUT: chatops_message
          )

          logger.info('Pipeline triggered on ChatOps', pipeline: pipeline.web_url)
        end

        def chatops_message
          ":mega: GitLab Security Release: #{versions.join(', ')} has just been released: #{blog_post_url}! Share this release blog post with your network to ensure broader visibility across our community."
        end

        def send_slack_notification(status)
          ReleaseTools::Slack::ReleaseJobEndNotifier.new(
            job_type: 'Notify Release complete',
            status: status,
            release_type: :security
          ).send_notification
        end

        def error_message
          <<~MSG
            The security release blog post couldn't be found or the pipeline in ChatOps failed to be created.

            Verify the blog post link is available at https://about.gitlab.com/releases/categories/releases/ and consider retrying this job.
            If the failure persists, manually send the notification via Slack:

            /chatops run notify "#{chatops_message}"
          MSG
        end
      end
    end
  end
end
