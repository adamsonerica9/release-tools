# frozen_string_literal: true

module ReleaseTools
  module Security
    module Finalize
      class BlogPostValidator
        include ::SemanticLogger::Loggable

        CouldNotFindBlogPostError = Class.new(StandardError)

        HTTP_SUCCESS = 200

        def initialize(version:)
          @version = version
        end

        def execute
          blog_post_url =
            "#{domain}/#{release_date}/#{security_release_type}-gitlab-#{formatted_version}-released/"

          logger.info('Validating blog post url', url: blog_post_url)

          response = Retriable.with_context(:api) do
            HTTP.get(blog_post_url)
          end

          raise CouldNotFindBlogPostError unless response.status == HTTP_SUCCESS

          blog_post_url
        end

        private

        attr_reader :version

        def domain
          'https://about.gitlab.com/releases'
        end

        def release_date
          Date.today.strftime('%Y/%m/%d')
        end

        def security_release_type
          if SharedStatus.critical_security_release?
            'critical-security-release'
          else
            'security-release'
          end
        end

        def formatted_version
          version.tr('.', '-')
        end
      end
    end
  end
end
