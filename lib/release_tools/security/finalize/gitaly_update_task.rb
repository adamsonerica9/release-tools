# frozen_string_literal: true

module ReleaseTools
  module Security
    module Finalize
      class GitalyUpdateTask
        include ::SemanticLogger::Loggable

        CouldNotUpdateError = Class.new(StandardError)

        PIPELINE_SCHEDULE = 'components:update_gitaly'

        # @param action [Sym] - Accepts :enable or :disable
        def initialize(action:)
          @project = ReleaseTools::Project::ReleaseTools
          @client = ReleaseTools::GitlabOpsClient
          @action = action
        end

        def execute
          logger.info('Updating gitaly pipeline schedule', description: pipeline_schedule.description, action: action)

          return if SharedStatus.dry_run?

          take_ownership_of_pipeline(pipeline_schedule.id)
          update_pipeline_schedule(pipeline_schedule.id)
          send_slack_notification(:success)
        rescue StandardError => ex
          logger.fatal(failure_message, error: ex)
          send_slack_notification(:failed)

          raise CouldNotUpdateError
        end

        private

        attr_reader :project, :client, :action

        def pipeline_schedule
          Retriable.with_context(:api) do
            client
              .pipeline_schedules(project)
              .find { |pipeline_schedule| PIPELINE_SCHEDULE == pipeline_schedule.description }
          end
        end

        def take_ownership_of_pipeline(pipeline_schedule_id)
          logger.info('Taking ownership of the pipeline schedule', pipeline_schedule: pipeline_schedule_id)

          Retriable.with_context(:api) do
            client.pipeline_schedule_take_ownership(
              project,
              pipeline_schedule_id
            )
          end
        end

        def update_pipeline_schedule(pipeline_schedule_id)
          Retriable.with_context(:api) do
            client.edit_pipeline_schedule(
              project.ops_path,
              pipeline_schedule_id,
              active: enabling_gitaly_update_task?
            )
          end
        end

        def send_slack_notification(status)
          ReleaseTools::Slack::ReleaseJobEndNotifier.new(
            job_type: job_type,
            status: status,
            release_type: :security
          ).send_notification
        end

        def job_type
          if enabling_gitaly_update_task?
            'Enable Gitaly update task'
          else
            'Disable Gitaly update task'
          end
        end

        def enabling_gitaly_update_task?
          action == :enable
        end

        def failure_message
          <<~MSG
            Updating Gitaly update task failed. This job may be retried. If this job continues to fail,
            the #{PIPELINE_SCHEDULE} schedule can be manually updated at
            https://ops.gitlab.net/gitlab-org/release/tools/-/pipeline_schedules.
          MSG
        end
      end
    end
  end
end
