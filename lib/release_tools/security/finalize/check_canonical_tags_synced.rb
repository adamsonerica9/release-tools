# frozen_string_literal: true

module ReleaseTools
  module Security
    module Finalize
      class CheckCanonicalTagsSynced
        include ::SemanticLogger::Loggable

        CouldNotCompleteError = Class.new(StandardError)
        UnsyncedTagsError = Class.new(StandardError)

        def initialize
          @versions = ReleaseTools::Versions.next_versions.map(&:previous_patch)
        end

        def execute
          logger.info('Checking for tags on canonical', versions: versions)

          raise UnsyncedTagsError if unsynced_tags.any?

          send_slack_notification(:success)
        rescue StandardError => ex
          logger.fatal(error_message, error: ex)
          send_slack_notification(:failed)

          raise CouldNotCompleteError
        end

        private

        attr_reader :versions

        def expected_tags
          versions.map do |version|
            "v#{version}-ee"
          end
        end

        def tags
          @tags ||= ReleaseTools::GitlabClient.tags(
            ReleaseTools::Project::GitlabEe,
            order_by: 'updated',
            sort: 'desc'
          ).map(&:name)
        end

        def unsynced_tags
          @unsynced_tags ||= expected_tags.reject do |tag|
            tags.include?(tag)
          end
        end

        def send_slack_notification(status)
          ReleaseTools::Slack::ReleaseJobEndNotifier.new(
            job_type: 'Check canonical tags',
            status: status,
            release_type: :security
          ).send_notification
        end

        def error_message
          if !unsynced_tags.nil? && unsynced_tags.any?
            <<~MSG
              The following tags were not synced back to canonical: #{unsynced_tags.join(', ')}
              The tags can be manually checked at https://gitlab.com/gitlab-org/gitlab/-/tags
            MSG
          else
            <<~MSG
              There was a problem checking if the tags for #{versions.join(', ')} were synced back to canonical.
              This job can be retried. If the job continues to fail, the tags can be manually verified at
              https://gitlab.com/gitlab-org/gitlab/-/tags
            MSG
          end
        end
      end
    end
  end
end
