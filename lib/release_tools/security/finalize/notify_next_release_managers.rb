# frozen_string_literal: true

module ReleaseTools
  module Security
    module Finalize
      class NotifyNextReleaseManagers
        include ::SemanticLogger::Loggable
        include ::ReleaseTools::Security::IssueHelper

        CouldNotNotifyError = Class.new(StandardError)

        def execute
          if security_tracking_issue.present?
            logger.info(
              'Notify next release managers about the next security release',
              issue: security_tracking_issue.iid
            )

            return if SharedStatus.dry_run?

            post_issue_note
            send_slack_notification(:success)
          else
            logger.fatal('Security tracking issue not found')

            raise CouldNotNotifyError
          end
        rescue StandardError => ex
          logger.fatal(failure_message, error: ex)

          send_slack_notification(:failed)

          raise CouldNotNotifyError
        end

        private

        def post_issue_note
          Retriable.with_context(:api) do
            client.create_issue_note(
              project,
              issue: security_tracking_issue,
              body: notification_message
            )
          end
        end

        def project
          @project ||= ReleaseTools::Project::GitlabEe
        end

        def client
          @client ||= ReleaseTools::GitlabClient
        end

        def schedule
          ReleaseTools::ReleaseManagers::Schedule.new
        end

        def next_release_managers
          @next_release_managers ||= schedule
            .usernames_for_version(next_minor_version).map do |username|
              "@#{username}"
            end
        end

        def next_minor_version
          ReleaseTools::Version.new(schedule.active_version.next_minor)
        end

        def notification_message
          <<~MSG.strip
            Hello #{next_release_managers.join(', ')}. Please set a due date for the next security release.
          MSG
        end

        def failure_message
          <<~MSG
            Notifying next release managers failed. Ensure gitlab-release-tools-bot has access to the gitlab-org/gitlab repository and retry this job.
            If the job continues to fail, notify release managers about the security release on #{security_tracking_issue.web_url}
          MSG
        end

        def send_slack_notification(status)
          ReleaseTools::Slack::ReleaseJobEndNotifier.new(
            job_type: 'Notify Next Release Managers',
            status: status,
            release_type: :security
          ).send_notification
        end
      end
    end
  end
end
