# frozen_string_literal: true

module ReleaseTools
  module Slack
    class ReleasePipelineStartNotifier
      include ::SemanticLogger::Loggable
      include ReleaseTools::Slack::Utilities
      include ::ReleaseTools::Slack::ReleasePipelineHelpers

      # stage - the stage of the security pipeline, start and finalize are supported
      # release_type - :security and :monthly are supported
      def initialize(stage:, release_type:)
        @stage = stage
        @release_type = release_type
      end

      def execute
        logger.info("Notifying the stage of a #{release_type} pipeline", stage: stage, release_manager: user_name, pipeline_url: pipeline_url)

        ReleaseTools::Slack::Message.post(
          channel: ReleaseTools::Slack::F_UPCOMING_RELEASE,
          message: fallback_message[stage],
          blocks: slack_blocks
        )
      end

      private

      attr_reader :stage, :release_type

      def fallback_message
        {
          start: start_message,
          finalize: finalize_message
        }
      end

      def start_message
        "Release manager #{user_name} has started a #{release_type} release, initial steps will now be run in #{pipeline_url}."
      end

      def finalize_message
        "Release manager #{user_name} has started the final steps of a #{release_type} release, these steps will be executed as part of #{pipeline_url}."
      end

      def slack_blocks
        [
          {
            type: 'section',
            text: ReleaseTools::Slack::Webhook.mrkdwn(section_block)
          },
          {
            type: 'context',
            elements: [clock_context_element]
          }
        ]
      end

      def section_block
        [].tap do |text|
          text << EMOJI[release_type]
          text << fallback_message[stage]
        end.join(' ')
      end
    end
  end
end
