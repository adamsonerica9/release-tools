# frozen_string_literal: true

module ReleaseTools
  module Slack
    class ReleaseJobEndNotifier
      include ::SemanticLogger::Loggable
      include Utilities
      include ::ReleaseTools::Slack::ReleasePipelineHelpers

      # job_type - A string indicating the job type that succeeded or failed
      # status - A symbol to denote if the job status, :success or :failed supported.
      # release_type - A symbol to denote the release type, :monthly and :security are supported.
      def initialize(job_type:, status:, release_type:)
        @job_type = job_type
        @status = status
        @release_type = release_type
      end

      def send_notification
        logger.info('Posting slack message', job_type: job_type, status: status)

        ReleaseTools::Slack::Message.post(
          channel: ReleaseTools::Slack::F_UPCOMING_RELEASE,
          message: fallback_message,
          blocks: slack_blocks
        )
      end

      private

      attr_reader :job_type, :status, :release_type

      def fallback_message
        if status == :success
          "#{job_type} job successfully executed - #{job_url}"
        else
          "#{job_type} job failed, review the job log for details - #{job_url}"
        end
      end

      def job_url
        ENV.fetch('CI_JOB_URL')
      end

      def slack_blocks
        [
          {
            type: 'section',
            text: ReleaseTools::Slack::Webhook.mrkdwn(section_block)
          },
          {
            type: 'context',
            elements: [clock_context_element]
          }
        ]
      end

      def section_block
        [].tap do |text|
          text << EMOJI[release_type]
          text << status_icon
          text << notification_message
        end.join(' ')
      end

      def status_icon
        if status == :success
          ':ci_passing:'
        else
          ':ci_failing:'
        end
      end

      def notification_message
        if status == :success
          "*#{job_type} job <#{job_url}|successfully> executed*"
        else
          "*#{job_type} job failed, review the <#{job_url}|job log> for details*"
        end
      end
    end
  end
end
