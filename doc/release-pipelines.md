# Release Pipelines

This document contains guidelines and best practices for adding to or creating new pipelines to automate
release processes.

## Slack notifications

Each job should notify release managers if it has succeeded or failed in `#f_upcoming_release`.

The `ReleaseTools::Slack::ReleaseJobEndNotifier` is used to generate these notifications for the
release pipelines.

Each pipeline should begin by notifying that the pipeline has started and who started it.
The `ReleaseTools::Slack::ReleasePipelineStartNotifier` is used to generate this notification.

## Ability to fail

Release managers need to be able to move forward with a security release even if a job has
failed. This means that when a job fails, it should not block future jobs from having the
ability to be run.

## Ability to run out of order

Different parts of the release should be able to be run out of order. For example, if, for
some reason, the `Prepare` stage of the security release pipeline is never started, release
managers should still have the ability to start a later stage such as the `Finalize` stage.

## Logging

All jobs should include instructions for how to complete the task manually if the job
fails. The easiest way to ensure these instructions are always logged on failure is to
`rescue StandardError` on the entry method for the job.

## Idempotency

Jobs should strive to be idempotent so release managers can retry them on failure.

## Current Pipelines

These are the current releases that use a release pipeline:

- Security release - Jobs are found in `security_release:<stage>` stages
